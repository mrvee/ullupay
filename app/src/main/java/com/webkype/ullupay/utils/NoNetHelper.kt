package com.webkype.mawateruser.util

import android.content.Context
import com.webkype.byaajpay.data.network.interceptors.NoConnectivityException
import com.webkype.byaajpay.data.network.interceptors.NoInternetException

class NoNetHelper {
    companion object{
        fun netStat(context: Context, t:Throwable){
            if (t is NoInternetException){
                StyleToast.coustomToast(context)
            }else if (t is NoConnectivityException){
                StyleToast.coustomToast(context)
            }
        }
    }
}