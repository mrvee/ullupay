package com.webkype.rollo.ui.util

import android.text.TextUtils

class StringUtil {
    companion object{
        fun capitalizeFirst(inPutString: String): String {
            var newString = ""
            return if (TextUtils.isEmpty(inPutString)) newString else {
                var i = 0
                for (a in inPutString.toCharArray()) {
                    newString += if (i == 0) Character.toUpperCase(a) else a
                    i++
                }
                newString
            }
        }

    }
}