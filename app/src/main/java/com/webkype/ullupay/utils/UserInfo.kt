package com.webkype.ullupay.utils

import android.content.Context

class UserInfo {

    companion object{

        fun setCityName(c:Context, cityName:String) {
            Preference.save(c, "cityName", cityName)
        }
        fun getCityName(c: Context):String {
            return Preference.getString(c, "cityName")
        }

        fun setCityId(c:Context, cityId:Int) {
            Preference.save(c, "cityId", cityId)
        }
        fun getCityId(c: Context):Int {
            return Preference.getInt(c, "cityId")
        }
    }
}