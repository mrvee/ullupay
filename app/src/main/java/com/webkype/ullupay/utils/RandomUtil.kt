package com.webkype.ullupay.utils

class RandomUtil {
    companion object{




        fun getRandomOfLength5()= rand(10000, 99999)

        private fun rand(start: Long, end: Long): Long {
            require(start <= end) { "Illegal Argument" }
            return (Math.random() * (end - start + 1)).toLong() + start
        }

    }
}