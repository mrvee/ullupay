package com.webkype.ullupay.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {
    final private static SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    final private static SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String formatDate(String dateToBeFormatted) {
//        SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MM-yyyy");
        String newDate = "";
        if (TextUtils.isEmpty(dateToBeFormatted))
            return newDate;
        Date date = null;
        try {
            date = sdfFrom.parse(dateToBeFormatted);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            newDate = sdfTo.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public static String formatDate(String oldDate, final int format_yyyy_mm_dd) {
//        2020-09-21
        final SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MM-yyyy");
        final SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
        String newDate = "";
        if (TextUtils.isEmpty(oldDate))
            return newDate;
        Date date = null;
        try {
            date = sdfFrom.parse(oldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            newDate = sdfTo.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDate;
    }
}
