package com.webkype.ullupay.utils

interface Listener {
    enum class Actions{
        CLICKED
    }
    fun onItemClicked(position:Int,action:Actions)

}