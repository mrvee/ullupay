package com.webkype.mawateruser.util

import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.webkype.ullupay.R

class StyleToast {
    companion object {
        fun coustomToast(context: Context, msg: String = "Network is not available") {
            try {
                if (context == null)
                    return;
                val layoutInflater: LayoutInflater = LayoutInflater.from(context)
                val layout: View = layoutInflater.inflate(R.layout.toast_layout, null)
                val textView: TextView = layout.findViewById(R.id.toastMessage);
                textView.setText(msg)

                val toast: Toast = Toast(context);
                toast.setView(layout);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.show();

            } catch (e: Exception) {
                Log.d("ToastException", "" + e.message)

            }

        }
        fun showMessageDialog( context:Context , msg:String,  listener: DialogInterface.OnClickListener) {
            val alertDialogBuilder = AlertDialog.Builder(context);
            alertDialogBuilder.setMessage(""+msg);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("OK",
                listener);
            alertDialogBuilder.show()
    }
        fun showMessageDialog(context:Context, msg:String, positiveListner: DialogInterface.OnClickListener, negativeeListner: DialogInterface.OnClickListener) {
            val alertDialogBuilder = AlertDialog.Builder(context);
            alertDialogBuilder.setMessage(""+msg);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("OK",
                positiveListner);
            alertDialogBuilder.show()
    }
    }
}