package com.webkype.byaajpay.ui.base

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.ullupay.data.network.model.User

abstract class BaseFragment<B : ViewBinding>(@LayoutRes private val layout: Int) : Fragment() {
    protected lateinit var progressDialog: ProgressDialog
    protected lateinit var binding: B
    protected var user: User?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layout, container, false)
        progressDialog = ProgressDialog(context);
        progressDialog.setMessage("Processing...")
        progressDialog.setCancelable(true)
        user = AppPref(context).getUser()

        return binding.root
    }
    override fun onResume() {
        super.onResume()
        user = AppPref(context).getUser()
    }

fun showProgress(message:String="",canCancel: Boolean=true) {
    progressDialog.setCancelable(canCancel)
    if (message.isNotEmpty())
        progressDialog.setMessage(message)
    progressDialog.show()
}

fun hideProgress() {
    progressDialog.dismiss()
}

}