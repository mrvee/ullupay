package com.webkype.byaajpay.ui.base

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewbinding.ViewBinding
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.ullupay.data.network.model.User

abstract class BaseActivity<B : ViewBinding>(@LayoutRes private val layout: Int) :
    AppCompatActivity() {
    protected lateinit var binding: B
    protected lateinit var progressDialog: ProgressDialog
    protected var user: User?=null
    protected lateinit var context: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layout);
        context = this;
        progressDialog = ProgressDialog(this);
        progressDialog.setMessage("Processing...")
        progressDialog.setCancelable(true)
        user = AppPref(context).getUser()
    }

    fun showProgress(message:String="",canCancel: Boolean=true) {
        progressDialog.setCancelable(canCancel)
        if (message.isNotEmpty())
            progressDialog.setMessage(message)
        progressDialog.show()
    }
    override fun onResume() {
        super.onResume()
        user = AppPref(context).getUser()
    }
    fun hideProgress() {
        progressDialog.dismiss()
    }


}