package com.webkype.byaajpay.ui.base

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

abstract class BaseDialogFragment<B : ViewBinding>(@LayoutRes private val layout: Int) :
    BottomSheetDialogFragment() {
    protected lateinit var progressDialog: ProgressDialog
    protected lateinit var binding: B
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layout, container, false)
        progressDialog = ProgressDialog(context);
        progressDialog.setMessage("Processing...")
        progressDialog.setCancelable(true)

        isCancelable =false
        return binding.root
    }

    fun showProgress(message: String = "") {
        if (message.isNotEmpty())
            progressDialog.setMessage(message)
        progressDialog.show()
    }

    fun hideProgress() {
        progressDialog.dismiss()
    }

}