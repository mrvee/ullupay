package com.webkype.ullupay.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.webkype.ullupay.R
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import kotlinx.android.synthetic.main.recharge_bill_item.view.*

class ServicesAdapter(val context: Context, val servicesList: ArrayList<SampleModel>) : RecyclerView.Adapter<ServicesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recharge_bill_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val serviceItem : SampleModel = servicesList[position]

        Glide.with(context).load(serviceItem.image)
            .thumbnail(Glide.with(context).load(R.drawable.image_placeholder))
            .into(holder.itemView.rechargeBillItemImage)

        holder.itemView.rechargeBillItemName.text = serviceItem.name
    }

    override fun getItemCount(): Int {
        return servicesList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}