package com.webkype.ullupay.ui.myAccount

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.ActivityWalletBinding

class WalletActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWalletBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.setContentView(this,R.layout.activity_wallet)
        binding.abWallet.actTitle.setText("UlluPay Wallet")
        binding.abWallet.backAccount.setOnClickListener { onBackPressed() }

     }
}