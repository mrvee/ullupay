package com.webkype.ullupay.ui.mobile_recharge

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.webkype.byaajpay.ui.base.BaseDialogFragment
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.billing.OperatorResp
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.FragmentOperatorsBinding
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import com.webkype.ullupay.utils.Listener
import kotlinx.android.synthetic.main.activity_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class OperatorsFragment :
    BaseDialogFragment<FragmentOperatorsBinding>(R.layout.fragment_operators) {
    private lateinit var mDataList: ArrayList<SampleModel>
    private var mobile: String? = null
    private var param2: String? = null
    private val TAG = "OperatorsFragment"
    private lateinit var operatorAdapter: SelectOperatorAdapter

    private lateinit var listner: FragmentListner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mobile = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    public fun setListener(callBack: FragmentListner) {
        this.listner = callBack
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDataList = ArrayList()

        operatorAdapter = SelectOperatorAdapter(context!!, mDataList, object : Listener {
            override fun onItemClicked(position: Int, action: Listener.Actions) {
               val model = mDataList[position]
                if (action == Listener.Actions.CLICKED) {
                    listner?.onClicked(model.name,model.desc)
                    dismiss()
                }
            }
        })
        binding.ivClose.setOnClickListener { dismiss() }

        binding.rvOperatorList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvOperatorList.adapter = operatorAdapter

        hitLoginApi()
    }

    private fun hitLoginApi() {
        val enqueue = RetrofitClient.instance(context!!).getOperators()
        showProgress()
        enqueue?.enqueue(object : Callback<OperatorResp?> {
            override fun onResponse(call: Call<OperatorResp?>, response: Response<OperatorResp?>) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {

                        val list = it.locations

                        for (dta in list) {
                            mDataList.add(SampleModel("", dta.name, dta.id))
                        }
                        operatorAdapter.notifyDataSetChanged()
                        it
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
                    }
                }
            }

            override fun onFailure(call: Call<OperatorResp?>, t: Throwable) {
                hideProgress()
                Log.d(TAG, "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            OperatorsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}