package com.webkype.ullupay.ui.mobile_recharge

import android.R.attr.name
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.webkype.ullupay.R


class GooglePayUtil {
    companion object {
        val GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user"
        val GOOGLE_PAY_REQUEST_CODE = 123
        val CLIENT_UPI = "8686869018@upi"
        fun payAmount(context: Activity, amount: String, note: String, ransactionId: String) {
            Log.e("main_c ", "ta $amount tn $note tr $ransactionId ");

            if (amount.isNullOrEmpty())
                return

            val uri = Uri.parse("upi://pay").buildUpon()
                .appendQueryParameter("pa", CLIENT_UPI)
                .appendQueryParameter("pn", "Ullupay")
                .appendQueryParameter("mc", "4814")
                //.appendQueryParameter("tid", "02125412")
                .appendQueryParameter("tr", ransactionId)
                .appendQueryParameter("tn", note)
                .appendQueryParameter("am", amount)
                .appendQueryParameter("cu", "INR")
                //.appendQueryParameter("refUrl", "blueapp")
                .build()
            val upiPayIntent = Intent(Intent.ACTION_VIEW)
            upiPayIntent.data = uri
//           upiPayIntent.setPackage(GOOGLE_PAY_PACKAGE_NAME)

            val chooserIntent = Intent.createChooser(upiPayIntent,"Pay with")

            try {
//                context.startActivityForResult(upiPayIntent, GOOGLE_PAY_REQUEST_CODE)
                context.startActivityForResult(chooserIntent, GOOGLE_PAY_REQUEST_CODE)
            } catch (e: Exception) {
//                Toast.makeText(context, "Google Pay app not found", Toast.LENGTH_LONG).show()
                Toast.makeText(context,
                    "No UPI App not found. please install one to continues",
                    Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }
        }
    }
}