package com.webkype.ullupay.ui.authentication

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.AuthResponse
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityMobileLoginBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileLoginActivity : BaseActivity<ActivityMobileLoginBinding>(R.layout.activity_mobile_login) {
private val TAG="MobileLoginActivity"
    private lateinit var viewModel : AuthenticationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this

        binding.mobileLoginEdit.requestFocus()
        binding.mobileLoginEdit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                if (binding.mobileLoginEdit.length() == 10) {
                    binding.proceedMobileLoginText.alpha = 1f
                    binding.proceedMobileLoginText.isEnabled = true
                } else {
                    binding.proceedMobileLoginText.alpha = 0.3f
                    binding.proceedMobileLoginText.isEnabled = false
                }
            }
        })

        binding.proceedMobileLoginText.setOnClickListener {
            val mobile = binding.mobileLoginEdit.text.toString()
            if(mobile.length != 10) {
                printMessage("Mobile number is required")
            return@setOnClickListener
            }
            hitLoginApi(mobile)
        }

//        setUpViewModel()
    }

    private fun hitLoginApi(mobile: String) {
        val enqueue = RetrofitClient.instance(context!!).loginSignUp(mobile)
        showProgress()
        enqueue?.enqueue(object : Callback<AuthResponse?> {
            override fun onResponse(call: Call<AuthResponse?>, response: Response<AuthResponse?>) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {
                        val otp = it.otp
                        val otpIntent = Intent(context, OtpActivity::class.java)
                        otpIntent.putExtra("mobile", mobile)
                        otpIntent.putExtra("otp", "" + otp)
                        startActivity(otpIntent)

                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
//                        Log.d(TAG, it?.msg)
                    }
                }
            }

            override fun onFailure(call: Call<AuthResponse?>, t: Throwable) {
                hideProgress()
                Log.d(TAG, "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

/*
    rivate fun setUpViewModel(){
        viewModel = ViewModelProvider(this).get(AuthenticationViewModel::class.java)


        viewModel.validationError.observe(this, Observer {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })
    }*/
}