package com.webkype.ullupay.ui.myAccount

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.mawateruser.util.StyleToast
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.CommonResponse
import com.webkype.ullupay.data.network.model.VerifyEmailResponse
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityKycBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KycActivity : BaseActivity<ActivityKycBinding>(R.layout.activity_kyc) {

    private val TAG = "KycActivity"
    private var kycType: String? = ""
    private var kycNumber: String? = ""
    private var fullName: String? = ""
    private var userId: String? = ""
    private var email: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.extras != null) {
            userId = intent.extras?.getString("userId");
            email = intent.extras?.getString("email");
        }
        binding.abKyc.actTitle.setText("KYC Status")
        binding.abKyc.backAccount.setOnClickListener { onBackPressed() }


        binding.rgType.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbPan -> kycType = "${binding.rbPan.text}"
                R.id.rbAadhar -> kycType = "${binding.rbAadhar.text}"
                R.id.rbVoter -> kycType = "${binding.rbVoter.text}"
                R.id.rbDl -> kycType = "${binding.rbDl.text}"
            }
        }
        binding.sbmitValues.setOnClickListener {
            val cardNumber = binding.etCardNumber.text.toString().trim()
            val cardNAme = binding.etCardName.text.toString().trim()
            if (TextUtils.isEmpty(kycType)) {
                printMessage("KYC is not selected")
                return@setOnClickListener
            } else if (TextUtils.isEmpty(cardNumber)) {
                printMessage("Card number is required")
                return@setOnClickListener
            } else if (TextUtils.isEmpty(cardNAme)) {
                printMessage("Card name is required")
                return@setOnClickListener
            } else if (!binding.cbTnc.isChecked) {
                printMessage("Terms and conditions is not selected")
                return@setOnClickListener
            }
            verifyEmail()
        }
    }

    private fun verifyEmail() {
        val enqueue =
            RetrofitClient.instance(this).addcustomerkyc(userId, kycType, kycNumber, fullName)
        showProgress()
        enqueue?.enqueue(object : Callback<CommonResponse?> {
            override fun onResponse(
                call: Call<CommonResponse?>,
                response: Response<CommonResponse?>,
            ) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {

                        StyleToast.showMessageDialog(this@KycActivity,
                            it.msg,
                            object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    dialog?.dismiss()
                                    onBackPressed()
                                }
                            })
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
                    }
                }
            }

            override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                hideProgress()
                Log.d(TAG, "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

}