package com.webkype.ullupay.ui.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.ActivityWebUrlLoaderBinding

class WebUrlLoaderActivity : BaseActivity<ActivityWebUrlLoaderBinding>(R.layout.activity_web_url_loader) {

    private val TAG = "WebViewLoaderActivity"
    private var title: String? = ""
    private var url: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.extras != null) {
            title = intent.extras?.getString("title");
            url = intent.extras?.getString("url");
        }

        binding.tbWebView.backAccount.setOnClickListener { onBackPressed() }
        binding.tbWebView.actTitle.text = "$title"

        binding.webView.getSettings().setJavaScriptEnabled(true)
        binding.webView.setWebChromeClient(MyWebChromeClient(binding.progressBar))
        binding.webView.setWebViewClient(webClient(binding.progressBar))
        binding.webView.loadUrl("$url")
    }

    class MyWebChromeClient(val progressBar: ProgressBar) : WebChromeClient() {
        override fun onProgressChanged(view: WebView, newProgress: Int) {
            progressBar.setVisibility(View.VISIBLE)
            progressBar.setProgress(newProgress)
        }
    }

    class webClient(val progressBar: ProgressBar) : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            progressBar.setVisibility(View.GONE)
        }
    }

    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
            binding.progressBar.setVisibility(View.GONE)
        } else {
            super.onBackPressed()
        }
    }

}