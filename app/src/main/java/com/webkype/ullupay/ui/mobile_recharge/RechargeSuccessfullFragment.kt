package com.webkype.ullupay.ui.mobile_recharge

import android.hardware.display.DisplayManager
import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.TranslateAnimation
import com.webkype.byaajpay.ui.base.BaseFragment
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.FragmentRechargeSuccessfullBinding

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class RechargeSuccessfullFragment :
    BaseFragment<FragmentRechargeSuccessfullBinding>(R.layout.fragment_recharge_successfull) {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ivClose.setOnClickListener {
            activity?.onBackPressed()
        }

//        binding.tv1.text="Recharge of ₹${RechargeUtil.amount} for Pre-Pade mobile ${RechargeUtil.mobile} is"
//        binding.ivCheck.setVisibility(View.GONE)
//        binding.ivClose.setVisibility(View.GONE)
//        binding.tv1.setVisibility(View.GONE)

//        loadTopDoenAnimation()


        binding.tvMobile.text="  : ${RechargeUtil.mobile}"
        binding.tvOperator.text="  : ${RechargeUtil.operator}"
        binding.tvRegion.text="  : ${RechargeUtil.region}"
        binding.tvRechargeAmount.text="  : ${RechargeUtil.amount}"

    }

    fun loadTopDoenAnimation(){

        val translate: Animation = TranslateAnimation(0f, 0f, -1000f, 76f)
        translate.duration = 1000
        translate.fillAfter = true
        val animationAlph: Animation = AlphaAnimation(0f, 1f,)
        animationAlph.duration = 300
        animationAlph.fillAfter = true
        val scene : AnimationSet= AnimationSet(true)

        scene.addAnimation(animationAlph)
        scene.addAnimation(translate)
        binding.ivCheck.startAnimation(scene)
        binding.ivCheck.setVisibility(View.VISIBLE)
//        animation.repeatMode = Animation.REVERSE

        translate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
             }
            override fun onAnimationRepeat(animation: Animation) {}
        })
    }

/*
    fun loadText1(view:View,toHeight:Float){
        val animation: Animation = TranslateAnimation(0f, 0f, 1000f, toHeight)
        animation.duration = 400
        animation.fillAfter = true
//        animation.repeatCount = 1
        view.startAnimation(animation)
        view.setVisibility(View.VISIBLE)

        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                loadText12(binding.tv2,100f)

            }
            override fun onAnimationRepeat(animation: Animation) {}
        })
    }
*/
    fun loadText12(view:View,toHeight:Float){
        val animation: Animation = TranslateAnimation(0f, 0f, 1000f, toHeight)
        animation.duration = 250
        animation.fillAfter = true
//        animation.repeatCount = 1
        view.startAnimation(animation)
        view.setVisibility(View.VISIBLE)

        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                loadCancelButton()
            }
            override fun onAnimationRepeat(animation: Animation) {}
        })
    }

    fun loadCancelButton(){

//        val animation: Animation = TranslateAnimation(0f, 0f, -1000f, 80f)
        val animation: Animation = AlphaAnimation(0f, 1f)
        animation.duration = 1000
        animation.fillAfter = true
        binding.ivClose.startAnimation(animation)
        binding.ivClose.setVisibility(View.VISIBLE)
//
    }

    companion object {
        val TAG = "RechargeSuccessfullFragment"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RechargeSuccessfullFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}