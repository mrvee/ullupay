package com.webkype.ullupay.ui.mobile_recharge

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.webkype.ullupay.databinding.ContactLayBinding
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import com.webkype.ullupay.utils.Listener
import kotlinx.android.synthetic.main.home_pager_item.view.*

class ContactsAdapter(
    val context: Context,
    val mList: ArrayList<SampleModel>,
    var contactListFiltered: List<SampleModel>,
    val listner: Listener,
) : RecyclerView.Adapter<ContactsAdapter.ViewHolder>(), Filterable {
    init {
        contactListFiltered = mList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bindig: ContactLayBinding =
            ContactLayBinding.inflate(LayoutInflater.from(context), parent, false)

        return ViewHolder(bindig)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bind: ContactLayBinding = (holder as ViewHolder).temBiew
        val model: SampleModel = contactListFiltered[position]
        bind.tvName.text = model.name
        bind.tvNumber.text = model.desc
        bind.contactMain.setOnClickListener {
            for (index in mList.indices) {
                val data = mList[index]
                if (data.desc == model.desc) {
                    listner.onItemClicked(index, Listener.Actions.CLICKED)
                    return@setOnClickListener
                }
            }
        }
    }


    override fun getFilter(): Filter? {

        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults? {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    contactListFiltered = mList
                } else {
                    val filteredList: MutableList<SampleModel> = ArrayList()
                    for (bank in mList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (bank.name?.toLowerCase()
                                ?.contains(charString.toLowerCase())!! || bank!!.name!!.contains(
                                charSequence)
                        ) {
                            filteredList.add(bank)
                        }
                    }
                    contactListFiltered = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = contactListFiltered
                return filterResults
            }

            protected override fun publishResults(
                charSequence: CharSequence?,
                filterResults: FilterResults,
            ) {
                contactListFiltered = filterResults.values as List<SampleModel>
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return contactListFiltered.size
    }

    class ViewHolder(public val temBiew: ContactLayBinding) :
        RecyclerView.ViewHolder(temBiew.root)
}