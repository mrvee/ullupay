package com.webkype.ullupay.ui.myAccount

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.ActivityAddAddressBinding

class AddAddressActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddAddressBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.setContentView(this,R.layout.activity_add_address)
        binding.abAddAddress.actTitle.setText("Add/Update Address")
        binding.abAddAddress.backAccount.setOnClickListener { onBackPressed() }

     }
}