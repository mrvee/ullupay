package com.webkype.ullupay.ui.myAccount

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.ActivityQrCodeBinding

class MyQrActivity : AppCompatActivity() {
    private lateinit var binding: ActivityQrCodeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.setContentView(this,R.layout.activity_qr_code)
        binding.abQr.actTitle.setText("My QR Code")
        binding.abQr.backAccount.setOnClickListener { onBackPressed() }

        Glide.with(this).load(R.drawable.qr_code)
            .into(binding.qrImage)


     }
}