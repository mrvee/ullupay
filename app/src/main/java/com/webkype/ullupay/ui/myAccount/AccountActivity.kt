package com.webkype.ullupay.ui.myAccount

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import coil.load
import coil.transform.CircleCropTransformation
import com.bumptech.glide.Glide
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.mawateruser.util.StyleToast.Companion.showMessageDialog
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.User
import com.webkype.ullupay.data.network.model.profile.ProfileResponse
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityAccountBinding
import com.webkype.ullupay.ui.authentication.MobileLoginActivity
import com.webkype.ullupay.ui.common.Constents
import com.webkype.ullupay.ui.common.WebUrlLoaderActivity
import kotlinx.android.synthetic.main.activity_account.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountActivity : BaseActivity<ActivityAccountBinding>(R.layout.activity_account) {
    var email: String = ""
    var kycType: String = ""
    var userId: String? = ""
    var isVerified: String = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.abAccount.actTitle.setText("Profile")
        binding.abAccount.actTitleTail.setText("LOGOUT")


        binding.abAccount.backAccount.setOnClickListener { onBackPressed() }
        binding.tvEditProfile.setOnClickListener {
            startActivity(Intent(this, EditProfileActivity::class.java))
        }

        binding.myAddress.setOnClickListener {
            startActivity(Intent(this, AddressActivity::class.java))
        }
        binding.myQrCode.setOnClickListener {
            startActivity(Intent(this, MyQrActivity::class.java))
        }
        binding.myUpiIds.setOnClickListener {
            startActivity(Intent(this, MyUpiIdsActivity::class.java))
        }

        binding.tvKycApply.setOnClickListener {
            val intent = Intent(context, KycActivity::class.java)
            intent.putExtra("email", "$email")
            intent.putExtra("userId", "$userId")
            startActivity(intent)
        }
        binding.tvBtnVerifyEmail.visibility = View.INVISIBLE
        binding.tvKycApply.visibility = View.INVISIBLE
        binding.tvBtnVerifyEmail.setOnClickListener {
            if (isVerified == "0" && email != null && email.isNotEmpty()) {
                val intent = Intent(context, VerifyEmailActivity::class.java)
                intent.putExtra("email", "$email")
                intent.putExtra("userId", "$userId")
                startActivity(intent)
            } else if (isVerified == "0" && email != null && email.isEmpty()) {
                startActivity(Intent(this, EditProfileActivity::class.java))
            }
        }

        binding.llPrivacyPolicy.setOnClickListener {
            val intent = Intent(context, WebUrlLoaderActivity::class.java)
            intent.putExtra("title", "Privacy Policy")
            intent.putExtra("url", "${Constents.privacyPolicy}")
            startActivity(intent)
        }
        binding.llHelp.setOnClickListener {
            val intent = Intent(context, WebUrlLoaderActivity::class.java)
            intent.putExtra("title", "Help")
            intent.putExtra("url", "${Constents.aboutUs}")
            startActivity(intent)
        }

        binding.abAccount.actTitleTail.setOnClickListener {
            showAskDialog()
        }
    }

    override fun onResume() {
        super.onResume()
        user?.let {
            binding.tvMobile.text = it.mobile
            binding.tvName.text = it.name
            binding.tvEmail.text = it.email

            if (it.logo.isNotEmpty())
                Glide.with(context).load(it.logo).into(binding.userImageAccount)
        }

        if (AppPref(context).getUser() != null) {
            userId = AppPref(context).getUser()?.uid
            getUserDetails(userId ?: "")
        }
    }


    private fun getUserDetails(userId: String) {
        val enqueue = RetrofitClient.instance(context!!).getProfileDetail(userId)
//        showProgress()
        enqueue?.enqueue(object : Callback<ProfileResponse?> {
            override fun onResponse(
                call: Call<ProfileResponse?>,
                response: Response<ProfileResponse?>,
            ) {
//                hideProgress()
                binding.tvBtnVerifyEmail.visibility = View.VISIBLE
                binding.tvKycApply.visibility = View.VISIBLE

                response.body().let {
                    if (it?.status == "200") {

                        val mobiel = it?.mobile
                        email = it?.emailid
                        val nameL = it?.name
                        val userLogo = it?.userlogo
                        val walletL = it?.wallet
                        kycType = it?.kyctype
                        val kycName = it?.kycfullname
                        val kycNumber = it?.kycnumber

                        isVerified = it?.isemail_verified

                        if (isVerified == "0")
                            binding.tvBtnVerifyEmail.visibility = View.VISIBLE
                        else if (isVerified == "1") {
                            binding.tvBtnVerifyEmail.isEnabled = false
//                            binding.tvBtnVerifyEmail.visibility = View.VISIBLE
                            binding.tvBtnVerifyEmail.text = "Verified"
                            binding.tvBtnVerifyEmail.setTextColor(resources.getColor(R.color.green))
                        } else {
                        }

                        if (kycType.isNullOrBlank()) {
                            binding.tvKycApply.isEnabled = true
                        } else if (TextUtils.isEmpty(kycName) && TextUtils.isEmpty(kycNumber)) {
                            binding.tvKycApply.isEnabled = true

                            binding.kycDetail.text = "$kycType Uploaded"
                            binding.tvKycApply.isEnabled = false
                            binding.tvKycApply.text = "Under Process"
                            binding.tvKycApply.setTextColor(resources.getColor(R.color.colorAccent))
                        } else {
                            binding.kycDetail.text = "$kycType Added"
                            binding.tvKycApply.isEnabled = false
                            binding.tvKycApply.text = "Verified"
                            binding.tvKycApply.setTextColor(resources.getColor(R.color.green))
                        }

                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
//                        Log.d(TAG, it?.msg)
                    }
                }
            }

            override fun onFailure(call: Call<ProfileResponse?>, t: Throwable) {
//                hideProgress()
                Log.d("DashBoardActivity", "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }


    fun showAskDialog() {
        val alertDialogBuilder = AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Are you sure you want to end the session?");
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.setPositiveButton("Yes", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                AppPref(context).logOut()
                startActivity(Intent(context, MobileLoginActivity::class.java))
                finishAffinity()
                dialog?.dismiss()
            }
        })
        alertDialogBuilder.setNegativeButton("No", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog?.dismiss()

            }
        })

        alertDialogBuilder.show()
    }

    override fun onBackPressed() {
        finish()
    }
}