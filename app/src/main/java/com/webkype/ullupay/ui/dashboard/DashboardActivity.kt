package com.webkype.ullupay.ui.dashboard

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import coil.load
import com.bumptech.glide.Glide
import com.webkype.ullupay.utils.UserInfo
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.User
import com.webkype.ullupay.data.network.model.profile.ProfileResponse
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityDashboardBinding
import com.webkype.ullupay.ui.myAccount.AccountActivity
import com.webkype.ullupay.ui.scanAndPay.ScanAndPayActivity
import com.webkype.ullupay.ui.userLocation.FetchLocationActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class DashboardActivity : BaseActivity<ActivityDashboardBinding>(R.layout.activity_dashboard) {

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var geocoder: Geocoder
    private var addresses: List<Address>? = null
    private var fragmentControl: String = ""
    private var doubleBackToExitPressedOnce = false
    private lateinit var fragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fetchLocation()

        bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        setUpFirstFragment()

        userImageDashboard.setOnClickListener {
            startActivity(Intent(this, AccountActivity::class.java))
        }

        dashboardLocLayout.setOnClickListener {
            startActivity(Intent(this, FetchLocationActivity::class.java))
        }

        if (AppPref(context).getUser()!=null){
            getUserDetails(AppPref(context).getUser()?.uid ?:"")
        }
    }
    private fun getUserDetails(userId: String) {
        val enqueue = RetrofitClient.instance(context!!).getProfileDetail(userId)
//        showProgress()
        enqueue?.enqueue(object : Callback<ProfileResponse?> {
            override fun onResponse(call: Call<ProfileResponse?>, response: Response<ProfileResponse?>) {
//                hideProgress()
                response.body().let {
                    if (it?.status == "200") {

                        val mobiel = it?.mobile
                        val usrid = it?.user_id
                        val emails = it?.emailid
                        val nameL = it?.name
                        val userLogo = it?.userlogo
                        val walletL = it?.wallet

                        val user = User().apply {
                            mobile = mobiel
                            uid = usrid
                            logo = userLogo
                            email = emails
                            name = nameL
                            wallet = walletL
                        }
                        AppPref(context).saveUser(user)
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
//                        Log.d(TAG, it?.msg)
                    }
                }
            }

            override fun onFailure(call: Call<ProfileResponse?>, t: Throwable) {
//                hideProgress()
                Log.d("DashBoardActivity", "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }


    override fun onStart() {
        super.onStart()
        if (AppPref(context).getUser()!=null){
            val userImage = AppPref(context).getUser()?.logo
            if (!userImage.isNullOrEmpty()){
                Glide.with(context).load(userImage).into(binding.userImageDashboard)
            }
        }
    }

    private fun setUpFirstFragment() {
        if (intent.extras != null) {
            fragmentControl = intent.getStringExtra("from")!!

            when (fragmentControl) {

                "home" -> {
                    fragment = HomeFragment()
                    loadFragment(fragment)
                    bottomNav.selectedItemId = R.id.actionHome
                }

                "services" -> {
                    fragment = ServicesFragment()
                    loadFragment(fragment)
                    bottomNav.selectedItemId = R.id.actionServices
                }

                "history" -> {
                    fragment = HistoryFragment()
                    loadFragment(fragment)
                    bottomNav.selectedItemId = R.id.actionHistory
                }
            }
        } else {
            fragment = HomeFragment()
            loadFragment(fragment)
            bottomNav.selectedItemId = R.id.actionHome
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.actionHome -> {
                    if (bottomNav.selectedItemId != R.id.actionHome) {
                        fragment = HomeFragment()
                        loadFragment(fragment)
                        return@OnNavigationItemSelectedListener true
                    } else {
                        return@OnNavigationItemSelectedListener false
                    }
                }

                R.id.actionServices -> {
                    if (bottomNav.selectedItemId != R.id.actionServices) {
                        fragment = ServicesFragment()
                        loadFragment(fragment)
                        return@OnNavigationItemSelectedListener true
                    } else {
                        return@OnNavigationItemSelectedListener false
                    }
                }

                R.id.actionScan -> {
                    if (bottomNav.selectedItemId != R.id.actionScan) {

                        startActivity(Intent(this, ScanAndPayActivity::class.java))
                        return@OnNavigationItemSelectedListener false
                    } else {
                        return@OnNavigationItemSelectedListener false
                    }
                }

                R.id.actionHistory -> {
                    if (bottomNav.selectedItemId != R.id.actionHistory) {
                        fragment = HistoryFragment()
                        loadFragment(fragment)
                        return@OnNavigationItemSelectedListener true
                    } else {
                        return@OnNavigationItemSelectedListener false
                    }
                }
            }
            false
        }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayoutDashboard, fragment)
        transaction.addToBackStack(null)
        transaction.commitAllowingStateLoss()
    }

    private fun fetchLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 100)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        } else {
            Toast.makeText(this, getString(R.string.location_permission_denied), Toast.LENGTH_LONG).show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            fusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->
                val location = task.result
                if (location != null) {
                    getCityName(location.latitude, location.longitude)
                } else {
                    dashboardCityName.text = getString(R.string.select_city)
                }
            }
        } else {
            startActivity(
                Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        }
    }

    private fun getCityName(latitude: Double, longitude: Double) {
        geocoder = Geocoder(this, Locale.getDefault())
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1)
            dashboardCityName.text = addresses?.get(0)?.locality

        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, getString(R.string.location_not_found), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRestart() {
        super.onRestart()
        //if location preference is not empty set city name else show "select city"
        if (UserInfo.getCityName(this).isNotEmpty()){
            dashboardCityName.text = UserInfo.getCityName(this)
        }
    }

    override fun onBackPressed() {
        if (bottomNav.selectedItemId != R.id.actionHome) {

            bottomNav.selectedItemId = R.id.actionHome
        } else {
            if (doubleBackToExitPressedOnce) {
                finishAffinity()
                return
            }
            doubleBackToExitPressedOnce = true
            Toast.makeText(this, getString(R.string.press_back_agin_exit), Toast.LENGTH_SHORT).show()
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }


}