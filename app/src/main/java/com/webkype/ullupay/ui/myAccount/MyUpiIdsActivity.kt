package com.webkype.ullupay.ui.myAccount

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.ActivityUpiIdsBinding

class MyUpiIdsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUpiIdsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.setContentView(this,R.layout.activity_upi_ids)
        binding.abUpi.actTitle.setText("My BHIM UPI IDs")
        binding.abUpi.backAccount.setOnClickListener { onBackPressed() }


     }
}