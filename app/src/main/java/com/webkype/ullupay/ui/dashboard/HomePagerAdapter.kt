package com.webkype.ullupay.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.webkype.ullupay.R
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import kotlinx.android.synthetic.main.home_pager_item.view.*

class HomePagerAdapter(val context: Context, val homePagerList: ArrayList<SampleModel>) : RecyclerView.Adapter<HomePagerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.home_pager_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(homePagerList[position].image).into(holder.itemView.homePagerImage)
    }

    override fun getItemCount(): Int {
       return homePagerList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}