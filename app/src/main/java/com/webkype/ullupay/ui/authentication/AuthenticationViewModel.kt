package com.webkype.ullupay.ui.authentication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AuthenticationViewModel : ViewModel() {

    var validationError = MutableLiveData<String>()

    fun mobileNoValidation(mobileNo : String) : Boolean{
        if(mobileNo.length < 10){
            validationError.value = "10 digit mobile number required"
            return false
        }
        return true
    }

}