package com.webkype.ullupay.ui.dashboard

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.home.HomeResp
import com.webkype.ullupay.data.network.model.transactions.TranModel
import com.webkype.ullupay.data.network.model.transactions.TransactionResp
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.FragmentHistoryBinding
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import droidninja.filepicker.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_history.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HistoryFragment : com.webkype.byaajpay.ui.base.BaseFragment<FragmentHistoryBinding>(R.layout.fragment_history) {

    private lateinit var mList: MutableList<TranModel>

    private val historyAdapter: HistoryAdapter by lazy {
        HistoryAdapter(mList,context!!)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mList= arrayListOf()
        binding.historyRecycler.isNestedScrollingEnabled = false
        binding.historyRecycler.isFocusable = false
        binding.historyRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.historyRecycler.adapter = historyAdapter
        getUserDetails()
    }
    private fun getUserDetails() {
        val userID = user?.uid
        val enqueue = RetrofitClient.instance(context!!).getAllTransaction(userID)
        showProgress()
        enqueue?.enqueue(object : Callback<TransactionResp?> {
            override fun onResponse(call: Call<TransactionResp?>, response: Response<TransactionResp?>) {
                hideProgress()
                mList.clear()
                response.body().let {
                    if (it?.status == "200") {
                        mList.addAll(it.data)
                        historyAdapter.notifyDataSetChanged()
                    } else {
                        historyAdapter.notifyDataSetChanged()
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
//                        Log.d(TAG, it?.msg)
                    }
                }
            }

            override fun onFailure(call: Call<TransactionResp?>, t: Throwable) {
                hideProgress()
                Log.d("DashBoardActivity", "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

}