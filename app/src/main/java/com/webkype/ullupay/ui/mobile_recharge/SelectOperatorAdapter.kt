package com.webkype.ullupay.ui.mobile_recharge

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.webkype.ullupay.databinding.OperatorItemBinding
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import com.webkype.ullupay.utils.Listener
import kotlinx.android.synthetic.main.home_pager_item.view.*

class SelectOperatorAdapter(
    val context: Context,
    val homePagerList: ArrayList<SampleModel>,
    val listner: Listener
) : RecyclerView.Adapter<SelectOperatorAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bindig: OperatorItemBinding =
            OperatorItemBinding.inflate(LayoutInflater.from(context), parent, false)

        return ViewHolder(bindig)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bind: OperatorItemBinding = (holder as ViewHolder).temBiew
        val model: SampleModel = homePagerList[position]
        bind.tvOperatorName.text = model.name
        bind.operatortMain.setOnClickListener { listner.onItemClicked(position,Listener.Actions.CLICKED) }
    }

    override fun getItemCount(): Int {
        return homePagerList.size
    }

    class ViewHolder(public val temBiew: OperatorItemBinding) :
        RecyclerView.ViewHolder(temBiew.root)
}