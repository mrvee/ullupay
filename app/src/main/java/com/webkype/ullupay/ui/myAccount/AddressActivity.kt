package com.webkype.ullupay.ui.myAccount

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.ActivityAddressBinding

class AddressActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddressBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.setContentView(this,R.layout.activity_address)
        binding.abAddress.actTitle.setText("My Address")
        binding.abAddress.backAccount.setOnClickListener { onBackPressed() }

        binding.addNewAddressRelative.setOnClickListener {
            startActivity(Intent(this,AddAddressActivity::class.java))
        }
     }
}