package com.webkype.ullupay.ui.dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.ui.base.BaseFragment
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.User
import com.webkype.ullupay.data.network.model.home.HomeResp
import com.webkype.ullupay.data.network.model.profile.ProfileResponse
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.FragmentHomeBinding
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import com.webkype.ullupay.ui.mobile_recharge.MobileRechargeActivity
import com.webkype.ullupay.ui.myAccount.WalletActivity
import com.webkype.ullupay.utils.Listener
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class HomeFragment : BaseFragment<FragmentHomeBinding>(R.layout.fragment_home) {

    private lateinit var fragmentContext: Context
    private var homePagerList = ArrayList<SampleModel>()
    private var homePagerList2 = ArrayList<SampleModel>()
    private var rechargeBillsList = ArrayList<SampleModel>()
    private var currentPage: Int = 1
    private var currentPage2: Int = 1
    private var timer: Timer? = null
    private var timer2: Timer? = null
    private var delayInMillis: Long = 2000 //delay in milliseconds before task is to be executed
    private var periodInMillis: Long =
        4000 // time in milliseconds between successive task executions.

    private val homePagerAdapter: HomePagerAdapter by lazy {
        HomePagerAdapter(context!!, homePagerList)
    }

    private val rechargeBillsAdapter: HomeRechargeBillsAdapter by lazy {
        HomeRechargeBillsAdapter(context!!, rechargeBillsList, object : Listener {
            override fun onItemClicked(position: Int, action: Listener.Actions) {
                if (action == Listener.Actions.CLICKED) {
                    when (rechargeBillsList[position].name) {
                        "Wallet Topup" -> goToWalletTopUpScreen()
                        "Mobile Recharge" -> goToMobileRechargeScreen()
                    }
                }
            }
        })
    }

    private fun goToMobileRechargeScreen() {
        startActivity(Intent(context, MobileRechargeActivity::class.java))
    }

    private fun goToWalletTopUpScreen() {
        startActivity(Intent(context, WalletActivity::class.java))
    }

    private val homePagerAdapter2: HomePagerAdapter by lazy {
        HomePagerAdapter(context!!, homePagerList2)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fragmentContext = context
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewPager.clipToPadding = false
        homeViewPager.clipChildren = false
        homeViewPager.offscreenPageLimit = 3
        homeViewPager.getChildAt(0).overScrollMode = View.OVER_SCROLL_NEVER
        homeViewPager.adapter = homePagerAdapter

        homeRechargeBillsRecycler.isNestedScrollingEnabled = false
        homeRechargeBillsRecycler.isFocusable = false
        homeRechargeBillsRecycler.layoutManager =
            StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL)
        homeRechargeBillsRecycler.adapter = rechargeBillsAdapter

        homeViewPager2.clipToPadding = false
        homeViewPager2.clipChildren = false
        homeViewPager2.offscreenPageLimit = 3
        homeViewPager2.getChildAt(0).overScrollMode = View.OVER_SCROLL_NEVER
        homeViewPager2.adapter = homePagerAdapter2


        getUserDetails()
    }

    private fun getUserDetails() {
        val enqueue = RetrofitClient.instance(context!!).getHomeData()
        showProgress()
        enqueue?.enqueue(object : Callback<HomeResp?> {
            override fun onResponse(call: Call<HomeResp?>, response: Response<HomeResp?>) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {
                        homePagerList.clear()
                        homePagerList2.clear()

                        viewPagerAnimation()
                        viewPagerAnimation2()
                        for (sta in it?.top_slider) {
                            homePagerList.add(SampleModel(sta.imageurl,sta.externallink,""))
                        }
                        for (sta in it?.bottom_slider) {
                            homePagerList2.add(SampleModel(sta.imageurl,sta.externallink,""))
                        }
                        setArrays()
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
//                        Log.d(TAG, it?.msg)
                    }
                }
            }

            override fun onFailure(call: Call<HomeResp?>, t: Throwable) {
                hideProgress()
                Log.d("DashBoardActivity", "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

    private fun setArrays() {
        rechargeBillsList.clear()
        rechargeBillsList.add(
            SampleModel(

                "https://e7.pngegg.com/pngimages/746/584/png-clipart-cryptocurrency-wallet-money-computer-icons-digital-wallet-wallet-payment-business-thumbnail.png",
                "Wallet Topup",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://cdn.iconscout.com/icon/free/png-256/mobile-recharge-1817169-1538037.png",
                "Mobile Recharge",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://pngimage.net/wp-content/uploads/2018/05/dth-icon-png-6.png",
                "DTH",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://cdn3.iconfinder.com/data/icons/energy-53/100/power_energy-07-512.png",
                "Electricity",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/credit_cards.png",
                "Credit Card Bill",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://icon-library.com/images/prepaid-icon/prepaid-icon-10.jpg",
                "Postpaid",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://marketdecisions.com/wp-content/uploads/2018/06/insurance-icon-10.png",
                "LIC/Insurance",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://cdn1.iconfinder.com/data/icons/business-rounded-vol-1/512/Loan_Money-512.png",
                "Loan Repayment",
                ""
            )
        )
        rechargeBillsList.add(
            SampleModel(
                "https://cdn4.iconfinder.com/data/icons/proglyphs-editor/512/Resize-512.png",
                "See All",
                ""
            )
        )
        rechargeBillsAdapter.notifyDataSetChanged()

    }

    private fun viewPagerAnimation() {
        homePagerAdapter.notifyDataSetChanged()
        homePagerIndicator.setViewPager(homeViewPager)

        val transformer = CompositePageTransformer()
        transformer.addTransformer(MarginPageTransformer(16))
        transformer.addTransformer { page, position ->
            val v = 1 - abs(position)
            page.scaleY = 0.8f + v * 0.2f
        }
        homeViewPager.setPageTransformer(transformer)

        timer = Timer()
        val handler = Handler()
        val update = Runnable {
            if (currentPage == homePagerList.size) {
                currentPage = 0
            }
            homeViewPager?.setCurrentItem(currentPage++, true)
        }
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, delayInMillis, periodInMillis)
    }

    private fun viewPagerAnimation2() {
        homePagerAdapter2.notifyDataSetChanged()
        homePagerIndicator2.setViewPager(homeViewPager2)

        val transformer = CompositePageTransformer()
        transformer.addTransformer(MarginPageTransformer(16))
        transformer.addTransformer { page, position ->
            page.scaleY = 0.8f + 0.2f
        }
        homeViewPager2.setPageTransformer(transformer)

        timer2 = Timer()
        val handler = Handler()
        val update = Runnable {
            if (currentPage2 == homePagerList2.size) {
                currentPage2 = 0
            }
            homeViewPager2?.setCurrentItem(currentPage2++, true)
        }
        timer2!!.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, delayInMillis, periodInMillis)
    }
}
