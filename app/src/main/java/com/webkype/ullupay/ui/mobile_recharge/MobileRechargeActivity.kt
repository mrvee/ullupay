package com.webkype.ullupay.ui.mobile_recharge

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.CommonResponse
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityMobileRechargeBinding
import com.webkype.ullupay.utils.RandomUtil
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileRechargeActivity :
    BaseActivity<ActivityMobileRechargeBinding>(R.layout.activity_mobile_recharge),
    PaymentResultListener {

    private lateinit var operatorListner: FragmentListner
    private lateinit var regionListner: FragmentListner
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        operatorListner = object : FragmentListner {
            override fun onClicked(firstValue: String, secondValue: String, thirdValue: String) {
                RechargeUtil.operator = firstValue
                RechargeUtil.operatorId = secondValue
                setCircleFragment(RechargeUtil.operator)
            }
        }

        regionListner = object : FragmentListner {
            override fun onClicked(firstValue: String, secondValue: String, thirdValue: String) {
                RechargeUtil.region = firstValue
                RechargeUtil.regionId = secondValue
                selectRechargePlanFragment()
            }
        }
        loadContactListFragment()

//        showSuccessfullPaymentFragment()
    }

    private fun loadContactListFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.rechargeHost, ContactFragment.newInstance("", ""))
            .addToBackStack(ContactFragment.TAG)
            .commit()
    }

    private fun selectRechargePlanFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.rechargeHost, AddRechargeAmountFragment.newInstance("", ""))
            .addToBackStack(AddRechargeAmountFragment.TAG)
            .commit()
    }

    fun goToPaymentGateWay(mobileNo: String, operator: String, region: String, amount: String) {
        val email = user?.email
        val mobile = user?.mobile
        startPayment(amount.toDouble(), email, mobile, "Mobile Recharge")
    }


    private fun startPayment(orderAmount: Double, email: String?, phone: String?, from: String) {
        val co = Checkout()
        try {
            val option = JSONObject()
            option.put("name", getString(R.string.app_name))
            option.put("description", from)
            option.put("image", "")
            option.put("currency", "INR")
            option.put("amount", (orderAmount * 100).toInt())
            val preFill = JSONObject()
            preFill.put("email", email)
            preFill.put("contact", phone)
            option.put("prefill", preFill)
            co.open(this, option)
        } catch (e: Exception) {
            Toast.makeText(this, "Error In Payment.Please try again!!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPaymentSuccess(s: String?) {
        Log.d("RAZER_pay", "token:${s}")
        rechargeMobile(RechargeUtil.mobile, RechargeUtil.operator, RechargeUtil.region, RechargeUtil.amount,s)
    }

    override fun onPaymentError(i: Int, s: String?) {
        Toast.makeText(context, "Transaction Cancelled", Toast.LENGTH_SHORT).show()
    }

    private fun rechargeMobile(
        mobileNo: String,
        operator: String,
        region: String,
        amount: String,
        transitionId: String?,
    ) {
//       var mobile =  "${mobileNo.substring(0,mobileNo.length-3)}000"
        val userId = user?.uid
        val uniqueNumber="$userId${RandomUtil.getRandomOfLength5()}"

        val enqueue = RetrofitClient.instance(context!!)
            .applyForRecharge(operator,
                region,
                mobileNo,
                amount,
                userId,
                transactionid = transitionId,
                uniqueid =uniqueNumber)
        showProgress("Don't press back... ", false)
        enqueue?.enqueue(object : Callback<CommonResponse?> {
            override fun onResponse(
                call: Call<CommonResponse?>,
                response: Response<CommonResponse?>,
            ) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {
                        showSuccessfullPaymentFragment()
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage("Money deducted successful.\nRecharge failed") }
                    }
                } ?: context?.printMessage("Server not responding")
            }

            override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                hideProgress()
                Log.d("PlanFragment", "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

    public fun showSuccessfullPaymentFragment() {
        supportFragmentManager.beginTransaction().commitAllowingStateLoss()
        val count: Int = supportFragmentManager.backStackEntryCount
        for (i in 0 until count) {
            supportFragmentManager.popBackStack()
        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.rechargeHost, RechargeSuccessfullFragment.newInstance("", ""))
            .addToBackStack(RechargeSuccessfullFragment.TAG)
            .commit()
    }

    public fun setOperatorFragment(mobile: String, name: String) {
        RechargeUtil.mobile = mobile
        val fragment = OperatorsFragment.newInstance(mobile, "")
        fragment.setListener(operatorListner)
        fragment.show(supportFragmentManager, "Operator")
    }

    public fun setCircleFragment(mobile: String) {
        val fragment = RegionsFragment.newInstance(mobile, "")
        fragment.setListener(regionListner)
        fragment.show(supportFragmentManager, "Circle")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (frag in supportFragmentManager.fragments) {
            frag.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else
            super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()
        Log.d("RAZER_pay", "randomNum:${RandomUtil.getRandomOfLength5()}")

    }
    override fun onDestroy() {
        RechargeUtil.clear()
        super.onDestroy()
    }
}