package com.webkype.ullupay.ui.dashboard

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import com.webkype.ullupay.R
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import kotlinx.android.synthetic.main.fragment_services.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class ServicesFragment : Fragment() {

    private lateinit var fragmentContext: Context
    private var servicePagerList =  ArrayList<SampleModel>()
    private var servicesList =  ArrayList<SampleModel>()
    private var currentPage : Int = 1
    private var timer: Timer? = null
    private var delayInMillis: Long = 2000 //delay in milliseconds before task is to be executed
    private var periodInMillis: Long = 4000 // time in milliseconds between successive task executions.

    private val homePagerAdapter: HomePagerAdapter by lazy {
        HomePagerAdapter(context!!, servicePagerList)
    }

    private val servicesAdapter: ServicesAdapter by lazy {
        ServicesAdapter(context!!, servicesList)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fragmentContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_services, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        serviceViewPager.clipToPadding = false
        serviceViewPager.clipChildren = false
        serviceViewPager.offscreenPageLimit = 3
        serviceViewPager.getChildAt(0).overScrollMode = View.OVER_SCROLL_NEVER
        serviceViewPager.adapter = homePagerAdapter

        servicesRecycler.isNestedScrollingEnabled = false
        servicesRecycler.isFocusable = false
        servicesRecycler.layoutManager = StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL)
        servicesRecycler.adapter = servicesAdapter

        setArrays()
    }

    private fun setArrays(){
        servicePagerList.clear()
        servicePagerList.add(SampleModel("https://offers.freecharge.in/NEW50Adapts/NEW30_LPBanner-1440x752.jpg", "", ""))
        servicePagerList.add(SampleModel("https://shoppingrechargeoffers.com/wp-content/uploads/2017/05/Axis-Bank-bill-payment-recharge.png", "", ""))
        servicePagerList.add(SampleModel("https://www.flashsaletricks.com/wp-content/uploads/2017/11/freecharge_vodafone_offer.jpg", "", ""))
        servicePagerList.add(SampleModel("https://i.pinimg.com/736x/99/b6/f7/99b6f773bf6994a9dfce624627d6c89b.jpg", "", ""))
        viewPagerAnimation()

        servicesList.clear()
        servicesList.add(SampleModel("https://cdn0.iconfinder.com/data/icons/medical-health-care-blue-series-set-1/64/a-12-512.png","Doctor Consultation",""))
        servicesList.add(SampleModel("https://image.freepik.com/free-vector/online-education-illustration-book-notes-smartphone-education-icon-concept-white-isolated_138676-637.jpg","Education",""))
        servicesList.add(SampleModel("https://dcassetcdn.com/design_img/3083544/696523/696523_17193459_3083544_4909a8bd_image.jpg","Entertainment",""))
        servicesList.add(SampleModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpVA80Z7WdagE7jZYg85NTgtV7u7luiBCiFA&usqp=CAU","Fitness & Sports",""))
        servicesList.add(SampleModel("https://cdn.iconscout.com/icon/free/png-256/fast-food-1851561-1569286.png","Food",""))
        servicesList.add(SampleModel("https://icons.iconarchive.com/icons/dtafalonso/android-lollipop/512/Play-Games-icon.png","Games",""))
        servicesList.add(SampleModel("https://cdn1.iconfinder.com/data/icons/supermarket-16/64/supermarket-grocery-store-food-512.png","Grocery",""))
        servicesList.add(SampleModel("https://cdn4.iconfinder.com/data/icons/medical-supplies-blue-set-1-1/100/Untitled-1-20-512.png","Pharmacy",""))
        servicesList.add(SampleModel("https://img.icons8.com/cotton/452/shopping.png","Shopping",""))
        servicesList.add(SampleModel("https://www.ybygear.com/wp-content/uploads/2020/06/world.png","Travel",""))
        servicesList.add(SampleModel("https://i.dlpng.com/static/png/1625437-download-high-quality-png-clean-home-cleaning-png-442_360_preview.png","Home Service",""))
        servicesList.add(SampleModel("https://allareaappliancellc.com/wp-content/uploads/2016/03/Vaccuum-Repair-e1531496293884.jpg","Appliance Repair",""))
        servicesList.add(SampleModel("https://cdn.iconscout.com/icon/free/png-256/automobile-car-carezhaust-environment-non-pollution-ecofriendly-5-33656.png","Automobile",""))
        servicesAdapter.notifyDataSetChanged()
    }

    private fun viewPagerAnimation(){
        homePagerAdapter.notifyDataSetChanged()
        servicePagerIndicator.setViewPager(serviceViewPager)

        val transformer = CompositePageTransformer()
        transformer.addTransformer(MarginPageTransformer(16))
        transformer.addTransformer { page, position ->
            val v = 1 - abs(position)
            page.scaleY = 0.8f + v * 0.2f
        }
        serviceViewPager.setPageTransformer(transformer)

        timer = Timer()
        val handler = Handler()
        val update = Runnable {
            if (currentPage == servicePagerList.size) {
                currentPage = 0
            }
            serviceViewPager?.setCurrentItem(currentPage++, true)
        }
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, delayInMillis, periodInMillis)
    }
}