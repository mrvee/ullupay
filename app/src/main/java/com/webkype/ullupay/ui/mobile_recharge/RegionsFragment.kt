package com.webkype.ullupay.ui.mobile_recharge

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.webkype.byaajpay.ui.base.BaseDialogFragment
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.billing.OperatorResp
import com.webkype.ullupay.data.network.model.region.RegionResp
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.FragmentOperatorsBinding
import com.webkype.ullupay.databinding.FragmentRegionsBinding
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import com.webkype.ullupay.utils.Listener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class RegionsFragment : BaseDialogFragment<FragmentRegionsBinding>(R.layout.fragment_regions)  {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var listner: FragmentListner
    private lateinit var operatorAdapter: SelectOperatorAdapter
    private lateinit var mDataList: ArrayList<SampleModel>
    private val TAG = "RegionsFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
    public fun setListener(callBack: FragmentListner) {
        this.listner = callBack
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDataList = ArrayList()

        operatorAdapter = SelectOperatorAdapter(context!!, mDataList, object : Listener {
            override fun onItemClicked(position: Int, action: Listener.Actions) {
                val model = mDataList[position]
                if (action == Listener.Actions.CLICKED) {
                    listner?.onClicked(model.name,model.desc)
                    dismiss()
                }
            }
        })
        binding.ivClose.setOnClickListener { dismiss() }

        binding.rvOperatorList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvOperatorList.adapter = operatorAdapter

        hitLoginApi()
    }
    private fun hitLoginApi() {
        val enqueue = RetrofitClient.instance(context!!).getRegions()
        showProgress()
        enqueue?.enqueue(object : Callback<RegionResp?> {
            override fun onResponse(call: Call<RegionResp?>, response: Response<RegionResp?>) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {

                        val list = it.locations

                        for (dta in list) {
                            mDataList.add(SampleModel("", dta.name, dta.id))
                        }
                        operatorAdapter.notifyDataSetChanged()
                        it
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
                    }
                }
            }

            override fun onFailure(call: Call<RegionResp?>, t: Throwable) {
                hideProgress()
                Log.d(TAG, "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegionsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RegionsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}