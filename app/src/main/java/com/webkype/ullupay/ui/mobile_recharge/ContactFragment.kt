package com.webkype.ullupay.ui.mobile_recharge

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.webkype.byaajpay.ui.base.BaseFragment
import com.webkype.byaajpay.util.printMessage
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.FragmentContactBinding
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import com.webkype.ullupay.utils.Listener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ContactFragment : BaseFragment<FragmentContactBinding>(R.layout.fragment_contact) {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var mDataList: ArrayList<SampleModel>
    private lateinit var contactListFiltered: ArrayList<SampleModel>

    private var name = ""
    private val colos = listOf<String>(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NUMBER,
        ContactsContract.CommonDataKinds.Phone._ID
    ).toTypedArray()

    // An adapter that binds the result Cursor to the ListView
    private var cursorAdapter: ContactsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Gets the ListView from the View list of the parent activity
        mDataList = arrayListOf()
        contactListFiltered = arrayListOf()

        cursorAdapter =
            ContactsAdapter(context!!, mDataList, contactListFiltered, object : Listener {
                override fun onItemClicked(position: Int, action: Listener.Actions) {
                    if (action == Listener.Actions.CLICKED) {
                        var mobile = mDataList[position].desc
                        mobile= mobile.replace("+91 ", "")
                        mobile=mobile.replace("+91", "")
                        mobile=mobile.replace(" ", "")
                        binding.etMobile.setText(mobile)
                        name = mDataList[position].name
                        if (binding.etMobile.length() == 10)
                            (activity as MobileRechargeActivity).setOperatorFragment(binding.etMobile.text.toString(),
                                name)
                    }
                }
            })
        binding.tbMbr.actTitle.text = "Mobile Recharge"
        binding.tbMbr.backAccount.setOnClickListener { activity?.onBackPressed() }

        binding.contactsList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        binding.tvbtnSubmit.setOnClickListener {
            val mobile = binding.etMobile.text.toString()
            if (mobile.isEmpty()) {
                context?.printMessage("Mobile is required")
                return@setOnClickListener
            } else if (mobile.length != 10) {
                context?.printMessage("Valid mobile is required")
                return@setOnClickListener
            }
            (activity as MobileRechargeActivity).setOperatorFragment(mobile, name)
        }

        binding.clearText.setOnClickListener {
            binding.etSearch.setText("")
            hideKeyboard(activity!!)
        }
        binding.etSearch.isEnabled = false

        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // filter recycler view when query submitted
                if (!s.isNullOrEmpty()) {
                    cursorAdapter?.getFilter()?.filter(s.toString())
                    binding.clearText.visibility = View.VISIBLE
                } else {
                    binding.clearText.visibility = View.GONE
                }
            }

            override fun afterTextChanged(s: Editable?) {
                // filter recycler view when text is changed
                cursorAdapter?.getFilter()?.filter(s.toString())
            }
        })

        checPermissions()
    }

    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun checPermissions() {
        if (ContextCompat.checkSelfPermission(context!!,
                android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(activity!!,
                arrayOf(android.Manifest.permission.READ_CONTACTS),
                100)
        } else {
            loadAllContacts()
        }

    }

    private fun loadAllContacts() {
        val rs =
            activity?.contentResolver?.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                colos, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
        showProgress("Loading contacts...")
        CoroutineScope(Dispatchers.IO).launch {
            val job = CoroutineScope(Dispatchers.IO).launch {
                while (rs?.moveToNext() == true) {
                    withContext(Dispatchers.Main) {
                        val name = rs.getString(0)
                        val number = rs.getString(1)
                        Log.d(TAG, "name :" + name)
                        Log.d(TAG, "number :" + number)
                        mDataList.add(SampleModel("", name, number))
                    }
                }
            }
            job.join()
            withContext(Dispatchers.Main) {
                binding.contactsList.adapter = cursorAdapter
                hideProgress()
                if (mDataList.size > 0)
                    binding.etSearch.isEnabled = true
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100 && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            loadAllContacts()
        } else {
            context?.printMessage("Permission Denied")
        }
    }

    companion object {
        val TAG = "ContactFragment"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ContactFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}