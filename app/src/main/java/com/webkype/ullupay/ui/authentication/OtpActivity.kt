package com.webkype.ullupay.ui.authentication

import `in`.aabhasjindal.otptextview.OTPListener
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.ui.dashboard.DashboardActivity
import com.webkype.ullupay.data.network.model.AuthResponse
import com.webkype.ullupay.data.network.model.User
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityOtpBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OtpActivity : BaseActivity<ActivityOtpBinding>(R.layout.activity_otp) {
    private val TAG = "OtpActivity"

    private var mobileNo: String = ""
    private var otpSent: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        init()
    }

    private fun init() {
        binding.backOtp.setOnClickListener {
            onBackPressed()
        }

        if (intent.extras != null) {
            mobileNo = intent.getStringExtra("mobile")!!
            otpSent = intent.getStringExtra("otp")!!

            binding.verifyOtpText.alpha = 0.3f
            binding.verifyOtpText.isEnabled = false
            binding.otpMobileNumber.text = mobileNo
        }

        binding.otpTextView.requestFocusOTP()
        binding.otpTextView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            override fun onOTPComplete(otp1: String) {
                // fired when user has entered the OTP fully.
                binding.verifyOtpText.alpha = 1f
                binding.verifyOtpText.isEnabled = true
            }
        }

        binding.resendOTP.setOnClickListener {
            binding.resendOTP.alpha = 0.3f
            binding.resendOTP.isEnabled = false

            Handler().postDelayed({
                binding.verifyOtpText.alpha = 0.3f
                binding.verifyOtpText.isEnabled = false

                binding.otpTextView.otp = ""
                binding.resendOTP.alpha = 1f
                binding.resendOTP.isEnabled = true
            }, 2000)
        }

        binding.verifyOtpText.setOnClickListener {

            val otp = binding.otpTextView.otp
            if (otp != otpSent) {
                printMessage("OTP not matched")
                return@setOnClickListener
            }
            hitLoginApi(mobileNo, otp)
        }
    }

    private fun hitLoginApi(mobileNo: String, otp: String) {
        val enqueue = RetrofitClient.instance(context!!).verifyOtp(mobileNo, otp)
        showProgress()
        enqueue?.enqueue(object : Callback<AuthResponse?> {
            override fun onResponse(call: Call<AuthResponse?>, response: Response<AuthResponse?>) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {
                        val mobiel = it?.mobile
                        val usrid = it?.uid
                        val user = User().apply {
                            mobile = mobiel ?: ""
                            uid = usrid ?: ""
                        }
                        AppPref(context).saveUser(user)

                        val otpIntent = Intent(context, DashboardActivity::class.java)
                        otpIntent.putExtra("from", "Auth")
                        otpIntent.putExtra("flag", "Auth")
                        startActivity(otpIntent)
                        finishAffinity()
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
                    }
                }
            }

            override fun onFailure(call: Call<AuthResponse?>, t: Throwable) {
                hideProgress()
                Log.d(TAG, "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

    override fun onBackPressed() {
        finish()
    }
}