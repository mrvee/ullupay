package com.webkype.ullupay.ui.myAccount

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import coil.clear
import coil.load
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.User
import com.webkype.ullupay.data.network.model.profile.ProfileResponse
import com.webkype.ullupay.data.network.network.ApiManager
import com.webkype.ullupay.data.network.network.FailureCodes
import com.webkype.ullupay.data.network.network.ResponseProgressListner
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityProfileBinding
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class EditProfileActivity : BaseActivity<ActivityProfileBinding>(R.layout.activity_profile),
    ResponseProgressListner {
    private var userID : String=""
    private var selectedFilePath: String? = ""
    private lateinit var filePaths: ArrayList<String?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.abEdtProf.actTitle.setText("Edit Profile")
        binding.abEdtProf.backAccount.setOnClickListener { onBackPressed() }
        binding.cardUpdate.setOnClickListener { validateField() }
        filePaths = arrayListOf()

        if (AppPref(context).getUser() != null) {
            userID = AppPref(context).getUser()?.uid ?: ""
            getUserDetails( userID)
        }
        binding.fmSelectImage.setOnClickListener { v ->
            requestAllPermission()
        }
    }

    private fun requestAllPermission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        getLocation()
                    }

                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // show alert dialog navigating to Settings
                        requestAllPermission()
                        //                             Toast.makeText(getApplicationContext(), "Permissions denied", Toast.LENGTH_SHORT).show();
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener { error ->
                Toast.makeText(
                    context,
                    "Error occurred! $error",
                    Toast.LENGTH_SHORT
                ).show()
            }
            .onSameThread()
            .check()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
    }

    private fun getLocation() {
        FilePickerBuilder.getInstance().setMaxCount(1)
            .setSelectedFiles(filePaths)
            .setActivityTheme(R.style.LibAppTheme)
            .pickPhoto(Objects.requireNonNull(this))
    }

    private fun validateField() {

        val name = binding.etName.text.toString()
        val mobile = binding.etMobile.text.toString()
        val email = binding.etEmail.text.toString()
        if (name.isNullOrEmpty()) {
            printMessage("Name is required")
            return
        }else if (mobile.isNullOrEmpty()) {
            printMessage("Mobile is required")
            return
        }else if (email.isNullOrEmpty()) {
            printMessage("Email is required")
            return
        }
        val bundle = Bundle()
        bundle.putString("name",name)
        bundle.putString("email",email)
        bundle.putString("mobile",mobile)
        bundle.putString("uid",userID)
        ApiManager.updateUser(this,bundle,selectedFilePath,this)
    }

    private fun getUserDetails(userId: String) {
        val enqueue = RetrofitClient.instance(context!!).getProfileDetail(userId)
//        showProgress()
        enqueue?.enqueue(object : Callback<ProfileResponse?> {
            override fun onResponse(
                call: Call<ProfileResponse?>,
                response: Response<ProfileResponse?>,
            ) {
//                hideProgress()
                response.body().let {
                    if (it?.status == "200") {

                        val mobiel = it?.mobile
                        val usrid = it?.user_id
                        val emails = it?.emailid
                        val nameL = it?.name
                        val userLogo = it?.userlogo
                        val walletL = it?.wallet
                        binding.etEmail.setText(emails)
                        binding.etName.setText(nameL)
                        binding.etMobile.setText(mobiel)

                        Glide.with(context).load(userLogo).into(binding.ivProfile)

                        val user = User().apply {
                            mobile = mobiel
                            uid = usrid
                            logo = userLogo
                            email = emails
                            name = nameL
                            wallet = walletL
                        }
                        AppPref(context).saveUser(user)

                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
//                        Log.d(TAG, it?.msg)
                    }
                }
            }

            override fun onFailure(call: Call<ProfileResponse?>, t: Throwable) {
//                hideProgress()
                Log.d("DashBoardActivity", "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }

    override fun onResponseInProgress() {
        binding.cardUpdate.setEnabled(false)

        showProgress("Uploading...")
    }

    override fun onResponseCompleted(response: Any?) {
        selectedFilePath = ""
        binding.cardUpdate.setEnabled(true)
        getUserDetails( userID)
        hideProgress()
    }

    override fun onResponseCompleted(response: Any?, bundle: Bundle?) {
        binding.cardUpdate.setEnabled(true)
        selectedFilePath = ""
        hideProgress()

    }

    override fun onResponseFailed(code: FailureCodes?) {
        binding.cardUpdate.setEnabled(true)
        hideProgress()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            FilePickerConst.REQUEST_CODE_PHOTO -> if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                filePaths.clear()
                Objects.requireNonNull(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA))
                    ?.let { filePaths.addAll(it) }
                selectedFilePath = filePaths.get(0)
                if (!selectedFilePath.isNullOrBlank()) {
                    Glide.with(context).load(selectedFilePath).into(binding.ivProfile)
                }
            }
        }
    }

}