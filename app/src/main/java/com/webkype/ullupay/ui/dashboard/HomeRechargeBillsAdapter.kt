package com.webkype.ullupay.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.webkype.ullupay.R
import com.webkype.ullupay.ui.dashboard.allModels.SampleModel
import com.webkype.ullupay.utils.Listener
import kotlinx.android.synthetic.main.recharge_bill_item.view.*

class HomeRechargeBillsAdapter(val context: Context, val rechargeBillsList: ArrayList<SampleModel>,val listener:Listener) : RecyclerView.Adapter<HomeRechargeBillsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.recharge_bill_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rechargeBillItem : SampleModel = rechargeBillsList[position]

        Glide.with(context).load(rechargeBillItem.image)
            .thumbnail(Glide.with(context).load(R.drawable.image_placeholder))
            .into(holder.itemView.rechargeBillItemImage)

        holder.itemView.rechargeBillItemName.text = rechargeBillItem.name
        holder.itemView.billItemMain.setOnClickListener { listener.onItemClicked(position,Listener.Actions.CLICKED) }

    }

    override fun getItemCount(): Int {
        return rechargeBillsList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}