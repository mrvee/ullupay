package com.webkype.ullupay.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.transactions.TranModel
import com.webkype.ullupay.databinding.HistoryItemBinding
import kotlinx.android.synthetic.main.history_item.view.*

class HistoryAdapter(val mData: List<TranModel>, val context: Context) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(HistoryItemBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bind =holder.itemView
        val mData= mData[position]

//        bind.historyPayReceiveIcon.setImageResource(R.drawable.receive_payment)
        bind.historyPayReceiveIcon.setImageResource(R.drawable.paid_payment)
        bind.historyAmountItem.text = "\u20b9${mData.amount}"
        bind.tvDate.text = mData.adddate
        bind.historyPayReceiveTxt.text = mData.trantype
        bind.historyDescTxt.text = mData.comment
        bind.historyAmtCdtDbtTxtItem.text = ""
//        bind.historyAmtCdtDbtImgItem.visibility = View.GONE
        bind.historyAmtCdtDbtImgItem.visibility = View.GONE

        bind.tvStatus.text = mData.rechargestatus
        if (mData.rechargestatus.toLowerCase()=="failed") {
            bind.tvStatus.setTextColor(context.resources.getColor(R.color.red))
        }else if (mData.rechargestatus.toLowerCase()=="success") {
            bind.tvStatus.setTextColor(context.resources.getColor(R.color.green))
        }
        /*if (position % 2 == 0){
            holder.itemView.historyPayReceiveIcon.setImageResource(R.drawable.receive_payment)
            holder.itemView.historyAmtCdtDbtTxtItem.text = context.getString(R.string.credited_to)
            holder.itemView.historyPayReceiveTxt.text = context.getString(R.string.received_from)
            holder.itemView.historyDescTxt.text = "Gajendra Singh"

            Glide.with(context).load("https://cdn.iconscout.com/icon/free/png-512/sbi-225865.png")
                .thumbnail(Glide.with(context).load(R.drawable.image_placeholder))
                .into(holder.itemView.historyAmtCdtDbtImgItem)
        } else{
            holder.itemView.historyPayReceiveIcon.setImageResource(R.drawable.paid_payment)
            holder.itemView.historyAmtCdtDbtTxtItem.text = context.getString(R.string.debited_from)
            holder.itemView.historyPayReceiveTxt.text = context.getString(R.string.paid_to)
            holder.itemView.historyDescTxt.text = "Sharma Groceries"

            Glide.with(context).load("https://pbs.twimg.com/profile_images/1121638315004874752/FiJuL1hN.png")
                .thumbnail(Glide.with(context).load(R.drawable.image_placeholder))
                .into(holder.itemView.historyAmtCdtDbtImgItem)
        }*/
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    class ViewHolder(val itemView: HistoryItemBinding) : RecyclerView.ViewHolder(itemView.root)
}