package com.webkype.ullupay.ui.myAccount

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import com.webkype.byaajpay.ui.base.BaseActivity
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.mawateruser.util.StyleToast.Companion.showMessageDialog
import com.webkype.ullupay.R
import com.webkype.ullupay.data.network.model.VerifyEmailResponse
import com.webkype.ullupay.data.network.network.RetrofitClient
import com.webkype.ullupay.databinding.ActivityVerifyEmailBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerifyEmailActivity :
    BaseActivity<ActivityVerifyEmailBinding>(R.layout.activity_verify_email) {
    private val TAG = ""
    private var userId: String? = ""
    private var email: String? = ""
    private var otpSent: Int? =0
    private var isOtpModeEnabled=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.extras != null) {
            userId = intent.extras?.getString("userId");
            email = intent.extras?.getString("email");
        }
        binding.tbEmailVerify.backAccount.setOnClickListener { onBackPressed() }
        binding.tbEmailVerify.actTitle.text = "Verify Email"


        binding.etEmail.setText("$email")
        binding.otpEmail.setText("$email")
        binding.llOtpVerify.visibility = View.GONE




        binding.tbBtnVerify.setOnClickListener {
            val email = binding.etEmail.text.toString().trim()

            if (isOtpModeEnabled){
                val otpRecived = binding.otpTextView.otp
                if (otpRecived != "$otpSent") {
                    printMessage("OTP not matched")
                    return@setOnClickListener
                }
                verifyOtp(userId)

            }else{

                if (email.length == 0) {
                    printMessage("Email is required")
                    return@setOnClickListener
                }
                verifyEmail(email)
            }

        }


    }

    private fun verifyEmail(email: String) {
        val enqueue = RetrofitClient.instance(context!!).verifyemail(email, userId)
        showProgress()
        enqueue?.enqueue(object : Callback<VerifyEmailResponse?> {
            override fun onResponse(
                call: Call<VerifyEmailResponse?>,
                response: Response<VerifyEmailResponse?>,
            ) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {
                        otpSent = it?.otp

                        showMessageDialog(this@VerifyEmailActivity,
                            it.msg,
                            object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    isOtpModeEnabled=true
                                    binding.llEmail.visibility = View.GONE
                                    binding.llOtpVerify.visibility = View.VISIBLE
                                    dialog?.dismiss()
                                }
                            })
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
                     }
                }
            }

            override fun onFailure(call: Call<VerifyEmailResponse?>, t: Throwable) {
                hideProgress()
                Log.d(TAG, "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }




    private fun verifyOtp(userId: String?) {
        val enqueue = RetrofitClient.instance(context!!).verifyEmailDone(userId)
        showProgress()
        enqueue?.enqueue(object : Callback<VerifyEmailResponse?> {
            override fun onResponse(
                call: Call<VerifyEmailResponse?>,
                response: Response<VerifyEmailResponse?>,
            ) {
                hideProgress()
                response.body().let {
                    if (it?.status == "200") {
                        otpSent = it?.otp
                        showMessageDialog(this@VerifyEmailActivity,
                            it.msg,
                            object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    dialog?.dismiss()
                                    onBackPressed()
                                }
                            })
                    } else {
                        it?.msg?.let { it1 -> context?.printMessage(it1) }
                    }
                }
            }

            override fun onFailure(call: Call<VerifyEmailResponse?>, t: Throwable) {
                hideProgress()
                Log.d(TAG, "" + t.message)
                NoNetHelper.netStat(context!!, t)
            }
        })
    }
}