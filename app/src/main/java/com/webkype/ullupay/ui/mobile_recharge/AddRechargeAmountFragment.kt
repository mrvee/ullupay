package com.webkype.ullupay.ui.mobile_recharge

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast

import com.webkype.byaajpay.ui.base.BaseFragment
import com.webkype.byaajpay.util.hideKeyboard
import com.webkype.byaajpay.util.printMessage
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.R
import com.webkype.ullupay.databinding.FragmentSelectaRechargePlanBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class AddRechargeAmountFragment :
    BaseFragment<FragmentSelectaRechargePlanBinding>(R.layout.fragment_selecta_recharge_plan) {
    private var param1: String? = null
    private var param2: String? = null
    private val MIN_AMOUNT = 1

    /*    private String midString ="Your Production mode MID here", txnAmountString="", orderIdString="", txnTokenString="";*/
//    private var midString: String = "uKiieU69219301879332" //test key
    private var midString: String = "" //production
    private var txnAmountString: String = ""
    private var orderIdString: String = "" //order id can be any unique number of length 4 to 10
    private var txnTokenString: String = "" //this will be generated from server
    private val ActivityRequestCode: Int = 2

/*
    private var transactionId: String = "" // for google payment gateway
*/
    private val TAG = "ADD_RECH"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.tbMbrPlan.actTitle.text = "Select a Recharge Plan"
        binding.tbMbrPlan.actTitle.text = "Recharge Amount"
        binding.tbMbrPlan.backAccount.setOnClickListener { activity?.onBackPressed() }

        binding.tvMobile.text = RechargeUtil.mobile
        binding.tvOperator.text = RechargeUtil.operator
        binding.tvRegion.text = RechargeUtil.region


        binding.tvbtnSubmit.setOnClickListener {
            val mobile = binding.tvMobile.text.toString()
            val operator = binding.tvOperator.text.toString()
            val region = binding.tvRegion.text.toString()
            val amount = binding.etRecharge.text.toString().trim()
            if (amount.isEmpty()) {
                context?.printMessage("Amount is required")
                return@setOnClickListener
            } else if (amount.toInt() < MIN_AMOUNT) {
                context?.printMessage("Minimum amount is $MIN_AMOUNT rupee ")
                return@setOnClickListener
            }
            RechargeUtil.amount = amount

            activity?.hideKeyboard()
            txnAmountString = amount

            (context as MobileRechargeActivity).goToPaymentGateWay(RechargeUtil.mobile,
                RechargeUtil.operator,
                RechargeUtil.region,
                RechargeUtil.amount)

//             getToken("${rand(1000000000, 9999999999)}")
//            GooglePayUtil.payAmount(activity!!, amount, "Mobile Recharge", transactionId)

        }
//        binding.tvMobile.text =RechargeUtil.mobile
    }

    /*fun getCurrentDate(): String {
        val calender = Calendar.getInstance()
        val sdf = SimpleDateFormat("ddMMyyyy")
        val date = sdf.format(calender.time)
        orderIdString = "${rand(1000000000, 9999999999)}"

        return date
    }*/


    fun rand(start: Long, end: Long): Long {
        require(start <= end) { "Illegal Argument" }
        return (Math.random() * (end - start + 1)).toLong() + start
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("main ", "response " + resultCode);

        when (requestCode) {
            /* GooglePayUtil.GOOGLE_PAY_REQUEST_CODE ->
                 if ((RESULT_OK == resultCode) || (resultCode == 11)) {
                     if (data != null) {
                         val trxt = data.getStringExtra("response");
                         Log.e("UPI", "onActivityResult: " + trxt);
                         val dataList = arrayListOf<String>();
                         if (trxt.isNullOrEmpty())
                             dataList.add("nothing");
                         else
                             dataList.add(trxt);
                         upiPaymentDataOperation(dataList);
                     } else {
                         Log.e("UPI", "onActivityResult: " + "Return data is null");
                         val dataList = arrayListOf<String>();
                         dataList.add("nothing");
                         upiPaymentDataOperation(dataList);
                     }
                 } else {
                     //when user simply back without payment
                     Log.e("UPI", "onActivityResult: " + "Return data is null");
                     val dataList = arrayListOf<String>();
                     dataList.add("nothing");
                     upiPaymentDataOperation(dataList);
                 }*/

            ActivityRequestCode -> if (requestCode == ActivityRequestCode && data != null) {
                val bundle = data.getExtras();
                if (bundle != null) {
                    for (key in bundle.keySet()) {
                        Log.e(TAG,
                            key + " : " + (bundle.get(key) != null ?: bundle.get(key) ?: "NULL"));
                    }
                }
                Log.e(TAG, " data " + data.getStringExtra("nativeSdkForMerchantMessage"));
                Log.e(TAG, " data response - " + data.getStringExtra("response"))
                /*
 data response - {"BANKNAME":"WALLET","BANKTXNID":"1394221115",
 "CHECKSUMHASH":"7jRCFIk6eRmrep+IhnmQrlrL43KSCSXrmM+VHP5pH0ekXaaxjt3MEgd1N9mLtWyu4VwpWexHOILCTAhybOo5EVDmAEV33rg2VAS/p0PXdk\u003d",
 "CURRENCY":"INR","GATEWAYNAME":"WALLET","MID":"EAcP3138556","ORDERID":"100620202152",
 "PAYMENTMODE":"PPI","RESPCODE":"01","RESPMSG":"Txn Success","STATUS":"TXN_SUCCESS",
 "TXNAMOUNT":"2.00","TXNDATE":"2020-06-10 16:57:45.0","TXNID":"2020061011121280011018328631290118"}
  */

//                rechargeMobile(RechargeUtil.mobile,RechargeUtil.operator,RechargeUtil.region,RechargeUtil.amount)
                Toast.makeText(context, data.getStringExtra("nativeSdkForMerchantMessage")
                        + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
            }

            else -> context?.printMessage("Transaction Failed")
        }
    }


    companion object {
        val TAG = "SelectaRechargePlanFragment"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddRechargeAmountFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}