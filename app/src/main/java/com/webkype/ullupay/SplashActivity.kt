package com.webkype.ullupay

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.ullupay.ui.authentication.MobileLoginActivity
import com.webkype.ullupay.ui.dashboard.DashboardActivity

class SplashActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        init()
    }

    private fun init() {
        context = this

        Handler().postDelayed({
            val user = AppPref(this).getUser();
            if (user != null) {
                startActivity(Intent(context, DashboardActivity::class.java))
            } else {
                startActivity(Intent(context, MobileLoginActivity::class.java))
            }
            finish()
        }, 2000)
    }

    override fun onBackPressed() {
    }
}