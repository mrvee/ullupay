package com.webkype.ullupay.data.network.network

 import okhttp3.MediaType
  import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*

class MultiPartHelperClass {
    companion object {
        fun getRequestBody(value: String?): RequestBody? {
            return if (!value!!.isEmpty() && value != null) {
                RequestBody.create(MediaType.parse("text/plain"), value)
            } else {
                RequestBody.create(MediaType.parse("text/plain"), "")
            }
        }

        fun getRequestFile(jsonFile: File, key: String?): MultipartBody.Part? {
            var body: MultipartBody.Part? = null
            if (jsonFile.exists()) {
                val requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), jsonFile)
                // MultipartBody.Part is used to send also the actual file name
                body = key?.let {
                    MultipartBody.Part.createFormData(
                        it,
                        getTimeinMillis().toString() + "_media.jpg",
                        requestFile
                    )
                }
            }
            return body
        }

        fun getMultipartData(imageFile: File, key: String?): MultipartBody.Part? {
            var body: MultipartBody.Part? = null
            if (imageFile.exists()) {
                val requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), imageFile)
                // MultipartBody.Part is used to send also the actual file name
                body = key?.let {
                    MultipartBody.Part.createFormData(
                        it,
                        getTimeinMillis().toString() + "_media.jpg",
                        requestFile
                    )
                }
            }
            return body
        }

        fun convertImagesToMultiPart(
                filePaths: ArrayList<String?>?,
                kay: String
        ): Array<MultipartBody.Part?>? {
            var size = 0
            size = if (filePaths != null && filePaths.size > 0) {
                filePaths.size
            } else {
                0
            }
            val surveyImagesParts = arrayOfNulls<MultipartBody.Part>(size)
            for (i in filePaths!!.indices) {
                val file = File(filePaths[i])
                MultipartBody.Part.createFormData(
                        "file", file.name, RequestBody.create(
                        MediaType.parse("image/*"), file
                    )
                )
                // multiPartList.add(filePart);
                val surveyBody = RequestBody.create(MediaType.parse("image/*"), file)
                /*Key Name - flyerimage*/surveyImagesParts[i] = MultipartBody.Part.createFormData(
                        "$kay[$i]", file.name, surveyBody
                )
            }
            return surveyImagesParts
        }

        fun getTimeinMillis(): Long {
            val cal = Calendar.getInstance()
            cal[cal[Calendar.YEAR], cal[Calendar.MONTH], cal[Calendar.DAY_OF_MONTH], cal[Calendar.HOUR_OF_DAY], cal[Calendar.MINUTE]] =
                    cal[Calendar.SECOND]
            return cal.timeInMillis
        }

       /* fun FiletoMultiPartBody(file: File?, type: String): MultipartBody.Part? {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file!!)
            return MultipartBody.Part.createFormData(type, file.name, requestFile)
        }*/
    }
}