package com.webkype.ullupay.data.network.model.billing

data class Location(
    val id: String,
    val name: String
)