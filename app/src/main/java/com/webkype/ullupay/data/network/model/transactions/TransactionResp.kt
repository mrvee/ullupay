package com.webkype.ullupay.data.network.model.transactions

data class TransactionResp(
    val `data`: List<TranModel>,
    val msg: String,
    val status: String
)