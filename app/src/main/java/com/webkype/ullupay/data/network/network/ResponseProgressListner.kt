package com.webkype.ullupay.data.network.network

import android.os.Bundle

interface ResponseProgressListner {
    fun onResponseInProgress()

    fun onResponseCompleted(response: Any?)
    fun onResponseCompleted(response: Any?,bundle:Bundle?)

    fun onResponseFailed(code: FailureCodes?)
}