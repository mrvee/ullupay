package com.webkype.ullupay.data.network.model

class AuthResponse(
    val msg: String, val status: String, val otp: Int, val uid: String,val mobile: String
)