package com.webkype.ullupay.data.network.network


import com.webkype.ullupay.data.network.model.AuthResponse
import com.webkype.ullupay.data.network.model.CommonResponse
import com.webkype.ullupay.data.network.model.VerifyEmailResponse
import com.webkype.ullupay.data.network.model.billing.OperatorResp
import com.webkype.ullupay.data.network.model.home.HomeResp
import com.webkype.ullupay.data.network.model.paytm_token.PaytmTokenResponse
import com.webkype.ullupay.data.network.model.profile.ProfileResponse
import com.webkype.ullupay.data.network.model.region.RegionResp
import com.webkype.ullupay.data.network.model.transactions.TransactionResp
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface Api {

    @Multipart
    @POST("update_user_profile.php/")
    fun updateUserProfile(
        @Part("uid") uid: RequestBody?,
        @Part("name") name: RequestBody?,
        @Part("emailid") emailid: RequestBody?,
        @Part("mobileno") mobile: RequestBody?,
        @Part uploadimage: MultipartBody.Part?,
    ): Call<CommonResponse?>?

    @FormUrlEncoded
    @POST("user_registration.php/")
    fun loginSignUp(
        @Field("mobileno") mobileno: String,
    ): Call<AuthResponse?>?



    @FormUrlEncoded
    @POST("verifyemail.php/")
    fun verifyemail(
        @Field("emailid") emailid: String?,
        @Field("uid") uid: String?,
    ): Call<VerifyEmailResponse?>?




    @FormUrlEncoded
    @POST("addcustomerkyc.php/")
    fun addcustomerkyc(
        @Field("uid") uid: String?,
        @Field("kyctype") kyctype: String?,
        @Field("kycnumber") kycnumber: String?,
        @Field("kycfullname") kycfullname: String?,
    ): Call<CommonResponse?>?



    @FormUrlEncoded
    @POST("confirmemail.php/")
    fun verifyEmailDone(
         @Field("uid") uid: String?,
    ): Call<VerifyEmailResponse?>?



    @FormUrlEncoded
    @POST("user_profile.php/")
    fun getProfileDetail(
        @Field("uid") uid: String,
    ): Call<ProfileResponse?>?

    @FormUrlEncoded
    @POST("verifyotp.php/")
    fun verifyOtp(
        @Field("mobileno") mobileno: String,
        @Field("otp") otp: String,
    ): Call<AuthResponse?>?

    @Multipart
    @POST("hh.php/")
    fun addCar(
        @Part("user_id") user_id: RequestBody?,
        @Part("brand_id") brand_id: RequestBody?,
        @Part("model_id") model_id: RequestBody?,
        @Part("model_year") model_year: RequestBody?,
        @Part("fuel_type") fuel_type: RequestBody?,
        @Part("model_varient") model_varient: RequestBody?,
    ): Call<CommonResponse?>?

    @GET("location_list.php/")
    fun getRegions(): Call<RegionResp?>?

    @GET("biller_list.php/")
    fun getOperators(): Call<OperatorResp?>?


    @GET("home_page_api.php/")
    fun getHomeData(): Call<HomeResp?>?

//mobile=https://ullupay.webkype.in/webservices/prepaid_recharge.php

/*billername:
location:
mobileno:
amount:
id:*/
    @FormUrlEncoded
    @POST("prepaid_recharge.php/")
    fun applyForRecharge(
        @Field("billername") billername: String,
        @Field("location") location: String,
        @Field("mobileno") mobileno: String,
        @Field("amount") amount: String,
        @Field("id") id: String?,
        @Field("uniqueid") uniqueid: String?,
        @Field("transactionid") transactionid: String?
    ): Call<CommonResponse?>?

    @FormUrlEncoded
    @POST("transaction_history.php/")
    fun getAllTransaction(
        @Field("user_id") user_id: String?
    ): Call<TransactionResp?>?


    @FormUrlEncoded
    @POST("pgtest.php/")
    fun getTransactionToken(
//        @Field("code") language: String,
//        @Field("MID") mid: String,
        @Field("refid") refid: String,
//        @Field("amount") amount: String,
     ): Call<PaytmTokenResponse?>?


    /* @GET("country_list.php/")
     fun getCountryList(): Call<StateListRes?>?

     @FormUrlEncoded
     @POST("state_list.php/")
     fun getStateList(@Field("country_id") country_id: String): Call<StateListRes?>?
 */
/*
    */
/*https://www.ullupay.webkype.in/webservices/city_list.php*//*

    @FormUrlEncoded
    @POST("city_list.php/")
    fun getCityList(@Field("state_id") state_id: String): Call<StateListRes?>?


    @FormUrlEncoded
    @POST("workshop_list.php/")
    fun getWorkShopList(
        @Field("user_id") vendor_id: String,
        @Field("latitude") latitude: String,
        @Field("longtitude") longtitude: String,
        @Field("cat_id") cat_id: String,
        @Field("startid") startid: String
    ): Call<WorkShopListResp?>?


    @FormUrlEncoded
    @POST("workshop_details.php/")
    fun getWorkShopDetail(
        @Field("workshop_id") workshop_id: String?,
        @Field("user_id") user_id: String?
    ): Call<WorkShopDetailResp?>?


    @FormUrlEncoded
    @POST("product_details.php/")
    fun getProductDetail(
        @Field("product_id") product_id: String?,
        @Field("user_id") user_id: String?
    ): Call<ProductDetailResp?>?
*/


    /*remainingUrl: "request_workshop.php", parameters: ["user_id" : NetworkManager.getUserId(),
    "workshop_id" : workshopId, "vendor_id" : vendorId,
     "latitude" : "", "longtitude" : "",
     "issue" : self.responseServiceData["data"][selectedIssueIndex] ["id"].stringValue,
      "carId" : self.responseData["car"] [selectedRow]["id"].stringValue,
      "address" : txtViewDetetctAddress.text ?? "",
       "description" : txtViewDescription.text ?? "" ],
        img: carImages, imgArrayKey: "images[]",
        imgName: "issueImage",*/
    @Multipart
    @POST("request_workshop.php/")
    fun bookWorkshop(
        @Part("workshop_id") workshop_id: RequestBody?,
        @Part("user_id") user_id: RequestBody?,
        @Part("vendor_id") vendor_id: RequestBody?,
        @Part("issue") issueID: RequestBody?,
        @Part("latitude") latitude: RequestBody?,
        @Part("longtitude") longtitude: RequestBody?,
        @Part("address") address: RequestBody?,
        @Part("carId") carId: RequestBody?,
        @Part("description") description: RequestBody?,
        @Part issueImage: Array<MultipartBody.Part?>?,
    ): Call<CommonResponse?>?
/*9) remainingUrl: "request_product.php", parameters:
   ["user_id": NetworkManager.getUserId(), "product_id":
   productId,"vendor_id": self.responseData["product"]
   ["vendor_id"].stringValue,"latitude":NetworkManager.getLa
   titude(),"longtitude":NetworkManager.getLongitude()]*/

    @FormUrlEncoded
    @POST("request_product.php/")
    fun requestProduct(
        @Field("user_id") user_id: String?,
        @Field("product_id") product_id: String?,
        @Field("vendor_id") vendor_id: String?,
        @Field("latitude") latitude: String?,
        @Field("longtitude") longtitude: String?,
    ): Call<CommonResponse?>?

/*
    @FormUrlEncoded
    @POST("model_list.php/")
    fun getModelList(@Field("brand_id") brand_id: String?): Call<BrandListResp?>?

    @GET("brand_list.php/")
    fun getBrandList(): Call<BrandListResp?>?

    @FormUrlEncoded
    @POST("car_variant_list.php/")
    fun getCarVarian(@Field("model_id") model_id: String?): Call<VariantListResp?>?*/

/*
    @FormUrlEncoded
    @POST("product_list.php/")
    fun getInventoryList(
        @Field("vendor_id") vendor_id: String?,
        @Field("cat_id") cat_id: String?,
        @Field("startid") startid: String
    ): Call<InventoryResp?>?

    @FormUrlEncoded
    @POST("part_cat_list.php/")
    fun getCategoryPart(@Field("category_id") category_id: String?): Call<PartListResp?>?

    @FormUrlEncoded
    @POST("part_sub_cat_list.php/")
    fun getSubCategoryPart(@Field("category_id") category_id: String?): Call<PartListResp?>?

    @FormUrlEncoded
    @POST("user_home.php/")
    fun getHomeData(@Field("user_id") user_id: String?): Call<HomeResp?>?

    @FormUrlEncoded
    @POST("user_product_request_list.php/")
    fun getProductRequest(
        @Field("user_id") user_id: String?,
        @Field("startid") startid: String
    ): Call<ProdReqResp?>?


    @FormUrlEncoded
    @POST("user_workshop_request_list.php/")
    fun getWorkShopRequestListBooking(
        @Field("user_id") user_id: String?,
        @Field("startid") startid: String
    ): Call<WorkShopRequestResp?>?
*/

/*
    *//*usertype:vendor or user*//*
    @FormUrlEncoded
    @POST("buyersellerchathistory.php/")
    fun getAllChatMessages(
        @Field("vendor_id") vendor_id: String?,
        @Field("user_id") user_id: String?,
        @Field("pro_shop_id") pro_shop_id: String?,
        @Field("pro_shop_type") pro_shop_type: String?,
        @Field("usertype") usertype: String?="user",
    ): Call<MessagesResp?>?*/

    /*pro_shop_id:13
    pro_shop_type:Workshop*/
    @Multipart
    @POST("sellerbuyerchat.php/")
    fun sendMessages(
        @Part("vendor_id") vendor_id: RequestBody?,
        @Part("user_id") user_id: RequestBody?,
        @Part("usertype") usertype: RequestBody?,
        @Part("message") message: RequestBody?,
        @Part("pro_shop_id") pro_shop_id: RequestBody?,
        @Part("pro_shop_type") pro_shop_type: RequestBody?,
        @Part chatimage: MultipartBody.Part?,
    ): Call<CommonResponse?>?
/*
    @FormUrlEncoded
    @POST("buyerchatlist.php/")
    fun getActiveUsersOfUser(@Field("user_id") user_id: String?): Call<ActiveChatUserResp?>?*/
/*
    @FormUrlEncoded
    @POST("car_list.php/")
    fun getCarList(@Field("user_id") user_id: String?): Call<CarList?>?*/

/*
    @FormUrlEncoded
    @POST("workshop_request_details.php/")
    fun getWorkShopRequestDetail(@Field("request_id") request_id: String?): Call<BookingRequestResp?>?*/


}