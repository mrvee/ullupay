package com.webkype.ullupay.data.network.model.home

data class BottomSlider(
    val bannername: String,
    val description: String,
    val externallink: String,
    val imageurl: String
)