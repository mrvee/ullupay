package com.webkype.ullupay.data.network.model.profile

data class ProfileResponse(
    val emailid: String,
    val mobile: String,
    val msg: String,
    val name: String,
    val status: String,
    val user_id: String,
    val userlogo: String,
    val wallet: String,
    val isemail_verified: String,
    val kyctype: String,
    val kycnumber: String,
    val kycfullname: String,
)