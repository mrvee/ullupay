package com.webkype.ullupay.data.network.model

class User (
    var mobile: String="",
    var uid: String="",
    var name: String="",
    var email: String="",
    var logo: String="",
    var wallet: String="",
)