package com.webkype.ullupay.data.network.model.region

data class Location(
    val id: String,
    val name: String
)