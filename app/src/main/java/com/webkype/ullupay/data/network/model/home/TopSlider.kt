package com.webkype.ullupay.data.network.model.home

data class TopSlider(
    val bannername: String,
    val description: String,
    val externallink: String,
    val imageurl: String
)