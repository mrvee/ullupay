package com.webkype.byaajpay.data.pref

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.webkype.ullupay.data.network.model.User

class AppPref(context: Context?) {
    private var preferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null
    init {
        if (preferences == null) {
            preferences = context?.getSharedPreferences(PrefConst.PREF_NAME, Context.MODE_PRIVATE)
        }
        editor = preferences?.edit()
    }

    fun saveUser(user: User?): Boolean {
        val userData = Gson().toJson(user)
        val isSaved = editor?.putString(PrefConst.USER, userData)?.commit()
        return isSaved ?: false;
    }


    fun getUser(): User? {
        val userData = preferences?.getString(PrefConst.USER, null)
        if (userData == null)
            return null
        else
            return Gson().fromJson(userData, User::class.java);
    }

   /* fun getVehicle(): Vechicle? {
        val userData = preferences?.getString(PrefConst.VAHICLE, null)
        if (userData == null)
            return null
        else
            return Gson().fromJson(userData, Vechicle::class.java);
    }
    fun saveVehicle(user: Vechicle?): Boolean {
        val userData = Gson().toJson(user)
        val isSaved = editor?.putString(PrefConst.VAHICLE, userData)?.commit()
        return isSaved ?: false;
    }

*/

    fun logOut(){
        editor?.clear()
        editor?.apply()
        editor?.putBoolean(PrefConst.IS_SKIPP, true)?.commit()
    }

    fun isSkipped() = preferences?.getBoolean(PrefConst.IS_SKIPP, false)
    fun skipIntro() = editor?.putBoolean(PrefConst.IS_SKIPP, true)?.commit()
companion object {
        var pref: SharedPreferences? = null

    }
}