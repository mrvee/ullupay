package com.webkype.ullupay.data.network.model.transactions

data class TranModel(
    val adddate: String,
    val amount: String,
    val comment: String,
    val trantype: String,
    val rechargestatus: String
)