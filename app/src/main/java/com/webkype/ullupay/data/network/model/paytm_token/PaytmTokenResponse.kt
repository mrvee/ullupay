package com.webkype.ullupay.data.network.model.paytm_token

class PaytmTokenResponse(
    val msg: String,
    val status: String,
    val accesscode: String,
    val orderid: String,
    val mid: String,
)