package com.webkype.ullupay.data.network.network

import android.content.Context
import com.webkype.byaajpay.data.network.interceptors.AppInterceptor
import com.webkype.byaajpay.data.network.interceptors.NoConnectionInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {
    companion object {
         private const val BASE_URL="https://ullupay.webkype.in/webservices/"
        private var retrofit: Retrofit? = null

        private var httpClient: OkHttpClient? = null
        fun instance(context: Context): Api {
            if (httpClient == null) {
                httpClient = getInterceptor(context)
            }
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build()
            }
            return retrofit!!.create(Api::class.java)
        }

        private fun getInterceptor(context: Context): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(AppInterceptor())
                .addInterceptor(NoConnectionInterceptor(context))
                .readTimeout(30, TimeUnit.MINUTES)
                .writeTimeout(30, TimeUnit.MINUTES)
                .build()
        }
    }
}