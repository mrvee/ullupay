package com.webkype.ullupay.data.network.model

 class VerifyEmailResponse(
    val msg: String, val status: String, val otp: Int,
)