package com.webkype.ullupay.data.network.model.billing

data class OperatorResp(
    val locations: List<Location>,
    val msg: String,
    val status: String
)