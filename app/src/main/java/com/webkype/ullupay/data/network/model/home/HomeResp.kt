package com.webkype.ullupay.data.network.model.home

data class HomeResp(
    val bottom_slider: List<BottomSlider>,
    val msg: String,
    val status: String,
    val top_slider: List<TopSlider>
)