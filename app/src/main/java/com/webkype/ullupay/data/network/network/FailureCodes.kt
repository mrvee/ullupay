package com.webkype.ullupay.data.network.network

enum class FailureCodes {
    NO_INTERNET,
    RESPONSE_NULL,
    API_ERROR ,
    STATUS_0
}