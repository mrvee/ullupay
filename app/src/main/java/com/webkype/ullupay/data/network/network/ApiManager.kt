package com.webkype.ullupay.data.network.network

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import com.webkype.byaajpay.data.pref.AppPref
import com.webkype.mawateruser.util.NoNetHelper
import com.webkype.ullupay.data.network.model.AuthResponse
import com.webkype.ullupay.data.network.model.CommonResponse
import com.webkype.ullupay.data.network.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ApiManager {

    companion object {
        val TAG = "ApiManager"

        fun updateUser(
            context: Context?,
            bundle: Bundle,
             profileLink: String?,
            listner: ResponseProgressListner
        ) {
            val api: Api = RetrofitClient.instance(context!!)
            var call = api.updateUserProfile(
                MultiPartHelperClass.getRequestBody(bundle.getString("uid", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("name", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("email", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("mobile", "")),
                MultiPartHelperClass.getMultipartData(File(profileLink), "uploadimage"),
            )
            listner.onResponseInProgress()
            call!!.enqueue(object : Callback<CommonResponse?> {
                override fun onResponse(
                    call: Call<CommonResponse?>,
                    response: Response<CommonResponse?>
                ) {
                    val updateProfile: CommonResponse? = response.body()
                    if (updateProfile?.status.equals("200")) {
                        Toast.makeText(context, "Updated Successfully", Toast.LENGTH_SHORT)
                            .show()
                        listner.onResponseCompleted(1)
                    } else if (updateProfile?.status.equals("400")) {
                        Toast.makeText(context, updateProfile?.msg, Toast.LENGTH_SHORT)
                            .show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    } else {
                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    }
                }

                override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                    Toast.makeText(
                        context,
                        "Unable to reach servers. Try Again !",
                        Toast.LENGTH_SHORT
                    ).show()
                    NoNetHelper.netStat(context, t)
                }
            })
        }

        fun sendMessage(
            context: Context?,
            bundle: Bundle,
            imageLInk: String?,
            listner: ResponseProgressListner
        ) {
            val api: Api = RetrofitClient.instance(context!!)
            var call = api.sendMessages(
                MultiPartHelperClass.getRequestBody(bundle.getString("vendorId", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("user_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("usertype", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("message", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("pro_shop_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("pro_shop_type", "")),
                MultiPartHelperClass.getMultipartData(File(imageLInk), "chatimage"),
            )
            listner.onResponseInProgress()
            call!!.enqueue(object : Callback<CommonResponse?> {
                override fun onResponse(
                    call: Call<CommonResponse?>,
                    response: Response<CommonResponse?>
                ) {
                    val updateProfile: CommonResponse? = response.body()
                    if (updateProfile?.status.equals("200")) {

                        listner.onResponseCompleted(1)
                    } else if (updateProfile?.status.equals("400")) {
                        Toast.makeText(context, updateProfile?.msg, Toast.LENGTH_SHORT)
                            .show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    } else {
                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    }
                }

                override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                    Toast.makeText(
                        context,
                        "Unable to reach servers. Try Again !",
                        Toast.LENGTH_SHORT
                    ).show()
                    NoNetHelper.netStat(context, t)
                }
            })
        }

        fun addWorkShopRequest(
            context: Context?,
            bundle: Bundle,
            imageList: ArrayList<String?>?,
            listner: ResponseProgressListner
        ) {
            val api: Api = RetrofitClient.instance(context!!)
            var call = api.bookWorkshop(
                MultiPartHelperClass.getRequestBody(bundle.getString("workshop_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("user_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("vendor_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("issue", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("latitude", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("longtitude", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("address", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("carId", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("description", "")),
                MultiPartHelperClass.convertImagesToMultiPart(imageList, "issueImage")
            )
            listner.onResponseInProgress()
            call!!.enqueue(object : Callback<CommonResponse?> {
                override fun onResponse(
                    call: Call<CommonResponse?>,
                    response: Response<CommonResponse?>
                ) {
                    val updateProfile: CommonResponse? = response.body()
                    if (updateProfile?.status.equals("200")) {

                        listner.onResponseCompleted(1)
                    } else if (updateProfile?.status.equals("400")) {
                        Toast.makeText(context, updateProfile?.msg, Toast.LENGTH_SHORT)
                            .show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    } else {
                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    }
                }

                override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                    Toast.makeText(
                        context,
                        "Unable to reach servers. Try Again !",
                        Toast.LENGTH_SHORT
                    ).show()
                    NoNetHelper.netStat(context, t)
                }
            })
        }

        fun saveInventory(
            context: Context?,
            bundle: Bundle,
            listner: ResponseProgressListner
        ) {
            val api: Api = RetrofitClient.instance(context!!)
            var call = api.addCar(
                MultiPartHelperClass.getRequestBody(bundle.getString("userId", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("brand_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("model_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("model_year", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("fuel_type", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("model_varient", "")),
            )
            listner.onResponseInProgress()
            call!!.enqueue(object : Callback<CommonResponse?> {
                override fun onResponse(
                    call: Call<CommonResponse?>,
                    response: Response<CommonResponse?>
                ) {
                    val updateProfile: CommonResponse? = response.body()
                    if (updateProfile?.status.equals("200")) {

                        listner.onResponseCompleted(1)
                    } else if (updateProfile?.status.equals("400")) {
                        Toast.makeText(context, updateProfile?.msg, Toast.LENGTH_SHORT)
                            .show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    } else {
                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    }
                }

                override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                    Toast.makeText(
                        context,
                        "Unable to reach servers. Try Again !",
                        Toast.LENGTH_SHORT
                    ).show()
                    NoNetHelper.netStat(context, t)
                }
            })
        }

        /*fun updateProfile(
            context: Context?,
            bundle: Bundle,
            imageLInk: String?,
            listner: ResponseProgressListner) {
            val api: Api = RetrofitClient.instance(context!!)
            var call: Call<LoginResp?>? = null
            call = api.updateUserDetails(
                MultiPartHelperClass.getRequestBody(bundle.getString("user_id", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("name", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("pancard", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("dob", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("address", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("pincode", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("city", "")),
                MultiPartHelperClass.getRequestBody(bundle.getString("gender", "")),
                MultiPartHelperClass.getMultipartData(File(imageLInk), "image")
            )
            listner.onResponseInProgress()
            call!!.enqueue(object : Callback<LoginResp?> {
                override fun onResponse(call: Call<LoginResp?>, response: Response<LoginResp?>) {
                    val resp: LoginResp? = response.body()
                    if (resp?.status.equals("200")) {
                       val userApi =resp?.user
                         Log.d(TAG, "Successfully Created")
                        val apUser = AppPref(context).getUser();
                        userApi.let {
                            apUser?.image=it?.image
                            apUser?.name=it?.name
                            apUser?.email=it?.email
                            apUser?.mobile=it?.mobile
                            apUser?.dob=it?.dob
                            apUser?.pancard=it?.pancard
                            apUser?.address=it?.address
                            apUser?.city=it?.city
                            apUser?.adddate=it?.adddate
                            apUser?.id=it?.id
                        }
                        AppPref(context).saveUser(apUser!!)
                        Toast.makeText(context, resp?.msg, Toast.LENGTH_SHORT)
                            .show()
                        listner.onResponseCompleted("done")
                    } else if (resp?.status.equals("400")) {
                        Toast.makeText(context, resp?.msg, Toast.LENGTH_SHORT)
                            .show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    } else {
                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show()
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL)
                    }
                }
                override fun onFailure(call: Call<LoginResp?>, t: Throwable) {
                    Toast.makeText(
                        context,
                        "Unable to reach servers. Try Again !",
                        Toast.LENGTH_SHORT
                    ).show()
                    NoNetHelper.netStat(context,t)
                }
            })
        }

        */
    }
}
