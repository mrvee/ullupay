package com.webkype.ullupay.data.network.model.region

data class RegionResp(
    val locations: List<Location>,
    val msg: String,
    val status: String
)